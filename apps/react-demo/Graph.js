import React from 'react';
import ReactDOM from 'react-dom';
import {Sigma, RandomizeNodePositions, RelativeSize, SigmaEnableWebGL} from 'react-sigma';
import ForceAtlas2 from 'react-sigma/lib/ForceAtlas2';

class Graph extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { nodesList, edgesList } = this.props
    const myGraph = { nodes: nodesList, edges: edgesList };
    console.log(nodesList)
    console.log(edgesList)

    return (<Sigma renderer="webgl"
              style={{maxWidth:"inherit", height:"600px"}}
              graph={myGraph} settings={{drawEdges: true, clone: true}}>
                <RelativeSize initialSize={15}/>
                <RandomizeNodePositions>
                  <ForceAtlas2
                      timeout={100}
                      linLogMode
                  />
                </RandomizeNodePositions>
            </Sigma>
    );
  }
}

const styles = {
  graph: {
    maxWidth: "inherit",
    height: "600px"
  },
  styleGraph: [
    {
      selector: 'node',
      style: {
        width: 20,
        height: 20,
        shape: 'rectangle'
      }
    },
    {
      selector: 'edge',
      style: {
        width: 15
      }
    }
  ]
}


const stylesheet = {
  layout: {
    name: 'random'
  }
}

export default Graph;
