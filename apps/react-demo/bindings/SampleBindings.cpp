#include <emscripten.h>
#include <emscripten/bind.h>
#include "Callbacks.h"
#include "Sample.h"


EMSCRIPTEN_BINDINGS(Sample) {
    emscripten::function("add", emscripten::optional_override([](int a, int b) -> int {
        return Sample::add(a, b);
    }));
    emscripten::class_<AnotherSample>("AnotherSample")
        .constructor<>()
        .function("saySomething", &AnotherSample::saySomething);
    emscripten::class_<MyTrieWebTest>("MyTrieWebTest")
        .constructor<>()
        .function("addWord", &MyTrieWebTest::addWord)
        .function("print", &MyTrieWebTest::print)
        .function("setNotificationCallback", &MyTrieWebTest::setNotificationCallback);
    emscripten::function("callJsBack", &callJsBack);
    emscripten::function("callJsBackWithAgrs", &callJsBackWithAgrs);
    emscripten::function("cppFunctionWithJsCallback", &cppFunctionWithJsCallback);
}
