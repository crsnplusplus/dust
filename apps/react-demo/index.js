import React from 'react';
import ReactDOM from 'react-dom';
import WasmTest from './WasmTest';

ReactDOM.render(<WasmTest />, document.getElementById('root'))
