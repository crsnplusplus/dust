import React, { Component } from 'react';
import Sample from './Sample.js';
import DustWASM from './Sample.wasm';
import Graph from './Graph.js'

class WasmTest extends Component {
  constructor(props) {
    super(props);

    var nodesList = []
    var edgesList = []
    nodesList.push({id:"n3", label:"Mad Hatter"})
    nodesList.push({id:"n4", label:"Cheshire Cat"})
    nodesList.push({id:"n5", label:"White Rabbit"})

    edgesList.push({id:"e2",source:"n1",target:"n3", label:"1"})
    edgesList.push({id:"e3",source:"n3",target:"n4", label:"2"})
    edgesList.push({id:"e4",source:"n4",target:"n2", label:"3"})
    edgesList.push({id:"e5",source:"n5",target:"n3", label:"4"})
    edgesList.push({id:"e6",source:"n5",target:"n2", label:"5"})
    edgesList.push({id:"e7",source:"n5",target:"n4", label:"6"})

    this.state = {
      dustLib: null,
      wordCount: 0,
      nodesList: nodesList,
      edgesList: edgesList
    };

    this.onClick = this.onClick.bind(this);
    this.callbackFromCpp = this.callbackFromCpp.bind(this);
  }

  componentDidMount() {
    const sample = Sample({
      locateFile: () => {
        return DustWASM;
      },
    }).then((core) => {

      var cpp = new core.MyTrieWebTest()
      cpp.setNotificationCallback(this.callbackFromCpp)
      cpp.addWord("Alice")
      this.setState({ dustLib: cpp })
      console.log("WASM Loaded")
    });
  }

  /*
      componentDidMount() {
          //const input = "./test/words_test_corriere.txt"
          const input = "./test/words.txt"
          const result = fetch(input)
              .then( response => response.text())
              .then( text => {
                  console.log("dictionary text ready")
                  new Sample({
                      locateFile: () => {
                          return DustWASM;
                      },
                  }).then((core) => {
                      var cpp = new core.MyTrieWebTest()
                      cpp.setNotificationCallback(this.callbackFromCpp)
                      const lines = text.split(/\r?\n/);
                      lines.forEach((line) => {
                          cpp.addWord(line)
                      });
                      this.setState({ dustLib: cpp })
                      console.log("Dust WASM Loaded")
                  });
              });
      }
  */
  componentWillUnmount() {
    this.state.dustLib.delete();
  }

  callbackFromCpp = () => {
    this.setState({ wordCount: this.state.wordCount + 1 })
  }

  onClick() {
    const { dustLib } = this.state 

    if (dustLib == null)
      return;

    //dustLib.print()
  }

  render() {
    const { dustLib } = this.state 

    if (dustLib == null) {
      return <div />
    }

    const { nodesList, edgesList } = this.state

    //this.setState({nodesList: nodesList, edgesList: edgesList})

    return (
      <div>
        <div>
          <h1>Hello!</h1>
        </div>
        <div>
          Word count is {this.state.wordCount}
        </div>
        <div>
          <button onClick={this.onClick}>Press me!</button>
        </div>
        <div>
          <Graph nodesList={nodesList} edgesList={edgesList}></Graph>
        </div>
      </div>
    );
  }
}

export default WasmTest;
