#pragma once

#include <emscripten.h>
#include <emscripten/bind.h>

#include <string>
#include <dust/ds/cx/trie.h>


class Sample {
public:
    static int add(int a, int b);  
};

class AnotherSample {
    public:
        std::string saySomething() const;
};

class MyTrieWebTest {
    public:
        MyTrieWebTest();
        void setNotificationCallback(emscripten::val callback);
        void addWord(std::string word);
        void print() const;
    
    private:
        dust::ds::cx::TrieNode<char> trie;
        emscripten::val notify;
};
