#include <memory>
#include <vector>
#include <dust/ds/cx/trie.h>

#include <string_view>

#include <unordered_map>

int main() {
    using namespace dust::ds::cx;
    using Node = TrieNode<char>;
    std::unique_ptr<Node> dict = std::make_unique<Node>();
    std::vector<std::string> words = {
        "francare", "francato", "francatura", "francature", "francesca", "francesca",
        "francescana", "francescane", "francescanesimi", "francescanesimo",
        "wolframio", "wolframite","woman","women","woofer","woolf","workshop","workstation","wrestling","writer",
        "wuerstel","wundt","wyeth","wyoming","xantato","xantene","xantenico","xanthi","xantica","xantiche","xantici",
        "xantico"
    };

    for (const std::string& s : words) {
        std::string s_terminated = s + ".";
        dict->insert(s_terminated);
    }
    
    dict->print();
    return 0;
}
