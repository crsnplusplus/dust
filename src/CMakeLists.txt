project(dust_lib)

add_library(dust_lib INTERFACE)
add_library(dust::lib ALIAS ${PROJECT_NAME})
target_include_directories(dust_lib INTERFACE ../include)

#add_executable(dust_lib_run main.cpp)
#target_include_directories(dust_lib_run PUBLIC ../include)
#target_link_libraries(dust_lib_run dust::lib)
