#pragma once

#include <cassert>
#include <string>
#include <vector>
#include <type_traits>

#include <iostream>
#include <optional>
#include <algorithm>
#include <functional>
#include <iterator>

#include <string>
#include <boost/variant/recursive_wrapper.hpp>
#include <boost/interprocess/allocators/node_allocator.hpp>

// containers
#include <unordered_map>
#include <tsl/hopscotch_map.h>
#include <tsl/robin_map.h>
#include <map>
#include <list>

namespace dust::ds::cx {

template <typename T>
class VisitorType {
  public:
    virtual void visit(T& n) = 0;
    virtual ~VisitorType() = default;
};

template <typename T>
class NodeVisitable {
  public:
    virtual void accept(VisitorType<T>& v) = 0;
    virtual ~NodeVisitable() = default;
};



template <typename T>
class TrieNode {
    public:
        using TrieNodeType = TrieNode<T>;
        using TrieNodePtr = std::unique_ptr<TrieNodeType>;
        using ContainerRM = tsl::robin_map<T, TrieNodePtr>;
        using ContainerHM = tsl::hopscotch_map<T, TrieNodePtr>;
        using ContainerOM = std::map<T, TrieNodePtr>;
        using ContainerUM = std::unordered_map<T, TrieNodePtr>;
        //using Container = ContainerOM; //typename std::conditional<UNORD, ContainerOM, ContainerUM>::type;
        using Container = ContainerRM;
        using ContainerIterator = typename Container::iterator;

    private:
        T m_value;
        Container m_tries;
        
    public:
        TrieNode() : m_value(T()) { }
        explicit TrieNode(const T& value) : m_value(value) { }
        TrieNode(const TrieNode& other) = default; // copy constructor
        TrieNode(TrieNode&& other) = default; // move constructor
        TrieNode& operator=(const TrieNode& other) { // copy assignment
          return *this = TrieNode(other);
        }
        TrieNode& operator=(TrieNode&& other) noexcept { // move assignment
            std::swap(m_value, other.m_value);
            std::swap(m_tries, other.m_tries);
            return *this;
        }
        virtual ~TrieNode() = default;

        T value() const { return m_value; }
        const Container& container() const { return m_tries; }

        template<typename Visitor>
        void accept(Visitor& v) {
          v.visit(*this);
        }

        template <typename It>
        void insert(It it, It itEnd) {
            if (it == itEnd) {
                return;
            }

            char value = *it;
            auto& container = m_tries[value];

            if (container == nullptr) {
                container = std::make_unique<TrieNodeType>(value);
            }

            container->insert(std::next(it), itEnd);
        }

        template <typename Container>
        void insert(const Container &container) {
            insert(std::begin(container), std::end(container));
        }

        void insert(const std::initializer_list<T> &il) {
            insert(std::begin(il), std::end(il));
        }

        void insert(const T m_value) {
            m_tries[m_value] = {};
        }

        void print(std::list<T> &l) const {

            for (const auto &p : m_tries) {
                l.push_back(p.first);

                if (p.second == nullptr) {
                  std::copy(std::begin(l), std::end(l), std::ostream_iterator<T>{std::cout, " "});
                  std::cout << std::endl;
                }
                else {
                  p.second->print(l);
                }

                l.pop_back();
            }
        }

        void print() const {
            std::list<T> l;
            print(l);
        }

        template <typename ItGenericContainer>
        TrieNodePtr subtrie(ItGenericContainer it, ItGenericContainer itEnd) {
            if (it == itEnd) {
                std::cout << "it == itEnd" << std::endl;
                TrieNodePtr last = std::make_unique<TrieNodeType>();
                return last;
            }

            auto found(m_tries.find(*it));
            if (found == std::end(m_tries)) {
                std::cout << "std::end(m_tries)" << m_value << " " << *it << std::endl;
                return nullptr;
            }
            std::cout << "found " << m_value << " " << *it << std::endl;

            TrieNodePtr copy = std::make_unique<TrieNodeType>(found->second->m_value);
            if (std::next(it) == itEnd) {
              Container& otherm_Tries = found->second->m_tries;
              for (auto& v : otherm_Tries) {
                //copy[v.first] = 
              }

            }
            else {
              copy->m_tries[found->second->m_value] = found->second->subtrie(std::next(it), itEnd);
            }
            return copy;
        }

        template <typename GenericContainer>
        auto subtrie(GenericContainer& c) { return subtrie(std::begin(c), std::end(c)); }
    };
}
