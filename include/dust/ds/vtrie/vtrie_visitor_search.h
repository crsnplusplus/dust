#pragma once

#include "vtrie.h"

#include <vector>

namespace dust::ds {
    template<typename key, key placeholder>
    class vtrie_visitor_search : public dust::ds::vtrie<key, placeholder>::visitor {
      public:
        typedef typename dust::ds::vtrie<key, placeholder> vtrie;
        typedef typename vtrie::node node;
        typedef typename vtrie::node_non_terminal node_non_terminal;
        typedef typename vtrie::node_terminal node_terminal;
        typedef typename std::vector<key> word_type;

      public:
        vtrie_visitor_search() { }
        virtual void visit(node_terminal*) {
          // done!
          m_found = true;
        }

        virtual void visit(node_non_terminal* n) {
          assert (m_pos < m_word.size());
          const key& k = m_word[m_pos];
          node* child = n->child(k);
          ++m_pos;

          if (child == nullptr)
            // not found, return
            return;
          else if (m_pos == m_word.size()) {
            // last one, checking terminator
            node* nt = child->child(placeholder);
            if (nt == nullptr)
              // no terminator, returning
              return;

            child = nt;
          }

          child->accept(this);
        }

        virtual ~vtrie_visitor_search() { }

        bool search(const word_type& word, vtrie* t) {
          assert(t != nullptr);
          assert(t->root() != nullptr);
          assert(word.size() > 0);

          m_word = word;
          m_pos = 0;
          m_found = false;

          node* root = t->root();
          root->accept(this);
          return m_found;
        }

      private:
        word_type m_word;
        size_t m_pos = 0;
        bool m_found = false;
    };

}
