#pragma once

#include "vtrie.h"

#include <vector>

namespace dust::ds {
    template<typename key, key placeholder>
    class vtrie_visitor_add_word : public dust::ds::vtrie<key, placeholder>::visitor {
      public:
        using vtrie = dust::ds::vtrie<key, placeholder>;
        using node = typename vtrie::node;
        using node_non_terminal = typename vtrie::node_non_terminal;
        using node_terminal = typename vtrie::node_terminal;
        using word_type = std::vector<key>;

      public:
        vtrie_visitor_add_word() { }
        virtual void visit(node_terminal* n) {
          assert (m_pos == m_word.size());
          assert (n->value() == placeholder);
          // done!
        }

        virtual void visit(node_non_terminal* n) {
          assert (m_pos < m_word.size());
          const key& k = m_word[m_pos];
          node* child = n->child(k);
          ++m_pos;

          if (child == nullptr) { // new node
              child = new node_non_terminal(k, n);
            n->add_child(k, child);
          }

          if (m_pos == m_word.size()) {
            // terminator check
            node* nt = child->child(placeholder);
            if (nt == nullptr) {
              nt = new node_terminal(child);
              child->add_child(placeholder, nt);
            }
            else {
              m_is_already_present = true;
            }
            child = nt;
          }

          child->accept(this);
        }

        virtual ~vtrie_visitor_add_word() { }

        bool add_word(const word_type& word, vtrie* t) {
          if (t == nullptr)
            return false;

          if (word.size() == 0)
            return false;

          m_word = word;
          m_pos = 0;
          m_is_already_present = false;

          node* root = t->root();
          root->accept(this);
          return m_is_already_present;
        }

      private:
        word_type m_word;
        size_t m_pos = 0;
        bool m_is_already_present = false;
    };

}
