#pragma once

#include <cassert>
#include <string>
#include <vector>
#include <type_traits>
#include <array>
#include <unordered_map>

namespace dust::ds {

    template <typename key_type, key_type placeholder_value>
    class vtrie {
            static_assert(std::is_arithmetic<key_type>::value, "key_type must be numeric");

        public:
            class node;
            class node_terminal;
            class node_non_terminal;
            class node_root;
            class visitor;

            using key = key_type;
            using word_storage = typename std::vector<key>;
            using container = std::unordered_map<vtrie::key, vtrie::node*>;

        public:
            class visitor {
                public:
                    virtual void visit(vtrie::node_terminal* node) = 0;
                    virtual void visit(vtrie::node_non_terminal* node) = 0;
                    virtual ~visitor() { }
            };

            class node {
                public:
                    explicit node(vtrie::key v, vtrie::node* p) noexcept : m_value(v), m_parent(p) { }

                    virtual void accept(visitor* v) = 0;
                    virtual void add_child(vtrie::key k, vtrie::node* n) = 0;
                    virtual vtrie::node* child(vtrie::key k) const = 0;
                    virtual ~node() { }

                    vtrie::node* parent() const noexcept { return m_parent; }
                    vtrie::key value() const noexcept { return m_value; }
                    bool is_root() const noexcept { return m_parent == nullptr; }

                private:
                    vtrie::key m_value;
                    vtrie::node* m_parent;
            };

            class node_non_terminal : public vtrie::node {
                public:
                    node_non_terminal(vtrie::key v, vtrie::node* p) : vtrie::node(v, p) { }
                    virtual ~node_non_terminal() {
                      for (auto n : m_children)
                        delete n.second;
                    }
                    virtual void accept(visitor* v) override {
                      v->visit(this);
                    }
                    virtual void add_child(vtrie::key k, vtrie::node* n) override {
                        assert(m_children.find(k) == m_children.end());
                        m_children[k] = n;
                    }
                    virtual vtrie::node* child(vtrie::key k) const override {
                         auto it = m_children.find(k);
                         return it == m_children.end() ? nullptr : it->second;
                    }

                private:
                    vtrie::container m_children;
            };

            class node_terminal : public vtrie::node {
                  typedef typename vtrie::node inherited;
                public:
                    node_terminal(vtrie::node* p, size_t id = 0)
                      : inherited(placeholder_value, p), m_id(id) { }
                    virtual void accept(visitor* v) override { v->visit(this); }
                    virtual void add_child(vtrie::key, vtrie::node*) override { assert(false); }
                    virtual vtrie::node* child(vtrie::key) const override { return nullptr; }
                    virtual ~node_terminal() { }

                private:
                    size_t m_id;
            };

        public:
            vtrie() : m_root(new node_non_terminal(vtrie::key(), nullptr)) { }
            virtual ~vtrie() { delete m_root; }

            vtrie::node* root() { return m_root; }
            consteval vtrie::key placeholder() const noexcept { return placeholder_value; }

        private:
            vtrie::node* m_root;
            std::string m_description;
    };
}
