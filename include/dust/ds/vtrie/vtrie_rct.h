#ifndef __VTRIE_RCT_H__
#define __VTRIE_RCT_H__

#include <iostream>
#include <unordered_map>
#include <stack>
#include <deque>
#include <memory>
#include <utility>
//#include <cassert>

#include <typeinfo>
#include <type_traits>


///////////////////////////////////////////////////////////////////////////////
// Virtual trie with nodes Root, Container and Terminator (rct)
namespace dust::trie::rct {
    template<typename TC> struct IVisitableNode;
    template<typename TC> struct NodeRoot;
    template<typename TC> struct NodeContainer;
    template<typename TC> struct NodeTerminator;

    template<typename TC>
    struct IVisitor {
        virtual void visitRoot(NodeRoot<TC>&) = 0;
        virtual void visitContainer(NodeContainer<TC>&) = 0;
        virtual void visitTerminator(NodeTerminator<TC>&) = 0;
        virtual ~IVisitor() {}
    };

    template<typename TC>
    struct IVisitableNode {
        using Key = typename TC::Key;
        using Value = typename TC::Value;
        using NodePtr = typename TC::Value;
        virtual ~IVisitableNode() {}

        virtual IVisitableNode* add(NodePtr&&) = 0;
        virtual Key key() const = 0;
        virtual IVisitableNode* parent() const = 0;
        virtual void setParent(IVisitableNode*) = 0;
        virtual bool contains(const Key key) const = 0;
        virtual Value& child(Key key) = 0;

        virtual void accept(IVisitor<TC>&) = 0;
    };

    template<typename TC>
    struct NodeRoot : public IVisitableNode<TC>
                    , skills::WithContainer<TC> {
        using Key = typename TC::Key;
        using Value = typename TC::Value;
        using Container = typename TC::ContainerType;
        using NodePtr = typename TC::Value;
        virtual ~NodeRoot() {}

        // IVisitableNode
        virtual void accept(IVisitor<TC>& v) override { v.visitRoot(*this); }
        virtual IVisitableNode<TC>* add(NodePtr&& n) override {
            auto key = n->key();
            n->setParent(this);
            return skills::WithContainer<TC>::add(key, std::move(n));
        }
        virtual Key key() const override { return 0; };
        virtual IVisitableNode<TC>* parent() const override { return nullptr; }
        virtual void setParent(IVisitableNode<TC>*) override { }
        virtual bool contains(const Key key) const override { return skills::WithContainer<TC>::contains(key); }
        virtual Value& child(Key key) override { return skills::WithContainer<TC>::get(key); }

        const Value& get(Key key) const { return skills::WithContainer<TC>::get(key); }
        void remove(const Key& key) { return skills::WithContainer<TC>::remove(key); }
        Container& container() { return skills::WithContainer<TC>::container(); }
        const Container& container() const { return skills::WithContainer<TC>::container(); }

        auto count() const { return skills::WithContainer<TC>::size(); }

        template<class Stranger>
        void acceptStranger(Stranger& stranger) {
            stranger.visit(*this);
        }
    };

    template<typename TC>
    struct NodeContainer : public IVisitableNode<TC>
                         , skills::WithParent<IVisitableNode<TC>>
                         , skills::WithContainer<TC>
                         , skills::WithKey<TC> {
        using Key = typename TC::Key;
        using Value = typename TC::Value;
        using Container = typename TC::ContainerType;
        using NodePtr = typename TC::Value;
        explicit NodeContainer(Key key)
            : skills::WithParent<IVisitableNode<TC>>{nullptr}
            , skills::WithContainer<TC>{}
            , skills::WithKey<TC>{key}
            { }
        virtual ~NodeContainer() { }

        // IVisitableNode
        virtual void accept(IVisitor<TC>& v) override { v.visitContainer(*this); }
        virtual IVisitableNode<TC>* add(NodePtr&& n) override {
            auto key = n->key();
            n->setParent(this);
            return skills::WithContainer<TC>::add(key, std::move(n));
        }
        virtual Key key() const override { return skills::WithKey<TC>::key(); };
        virtual IVisitableNode<TC>* parent() const override { return skills::WithParent<IVisitableNode<TC>>::parent(); }
        virtual void setParent(IVisitableNode<TC>* p) override { skills::WithParent<IVisitableNode<TC>>::setParent(p); }
        virtual bool contains(const Key key) const override { return skills::WithContainer<TC>::contains(key); }
        virtual Value& child(Key key) override { return skills::WithContainer<TC>::get(key); }
        
        const Value& get(Key key) const { return skills::WithContainer<TC>::get(key); }
        void remove(const Key& key) { return skills::WithContainer<TC>::remove(key); }
        Container& container() { return skills::WithContainer<TC>::container(); }
        const Container& container() const { return skills::WithContainer<TC>::container(); }

        template<class StrangerVisitor>
        void acceptStranger(StrangerVisitor& stranger) {
            stranger.visit(*this);
        }
    };

    template<typename TC>
    struct NodeTerminator : public IVisitableNode<TC>
                          , skills::WithParent<IVisitableNode<TC>> {
        using Key = typename TC::Key;
        using Value = typename TC::Value;
        using Container = typename TC::ContainerType;
        using NodePtr = typename TC::Value;
        //virtual ~NodeTerminator() { std::cout << "~NodeTerminator" << std::endl; }
        virtual ~NodeTerminator() { }

        // IVisitableNode
        virtual void accept(IVisitor<TC>& v) override { v.visitTerminator(*this); }
        virtual IVisitableNode<TC>* add(NodePtr&&) override { return nullptr; }
        virtual Key key() const override { return 0; };
        virtual IVisitableNode<TC>* parent() const override { return skills::WithParent<IVisitableNode<TC>>::parent(); }
        virtual void setParent(IVisitableNode<TC>* p) override { skills::WithParent<IVisitableNode<TC>>::setParent(p); }
        virtual bool contains(const Key key) const override { return false; }
        virtual Value& child(Key key) override { return m_value; }

        template<class Stranger>
        void acceptStranger(Stranger& stranger) {
            stranger.visit(*this);
        }

        Value m_value;
    };
};

///////////////////////////////////////////////////////////////////////////////
// visitors for trie
namespace dust::trie::visitors {
  
    template<class TC, class Factory, class SequenceContainer>
    class VisitorRCTInsertSequence : public virtual trie::rct::IVisitor<TC> {
            using Size = typename SequenceContainer::size_type;
            using NodePtr = typename TC::Value;
            using VisitableNode = rct::IVisitableNode<TC>;
            using SequenceElement = typename SequenceContainer::value_type;
            using Root = typename TC::Root;
            using Container = typename TC::Container;
            using Terminator = typename TC::Terminator;
        public:
            VisitorRCTInsertSequence(SequenceElement s) : m_terminator(s) {}
            virtual void visitRoot(rct::NodeRoot<TC>& root) override {
                Size size = std::size(m_sequence);
                VisitableNode* current = &root;

                for (Size position = 0; position < size; ++position) {
                    auto key = m_sequence.at(position);
                    assert(key != m_wcSingle);
                    assert(current != nullptr);

                    if (current->contains(key) == false) {
                        auto rawNode = current;
                        auto newNode = Factory::template make<NodePtr, Container>(key);
                        current = current->add( std::move(newNode) );
                    }
                    else {
                        NodePtr& next = current->child(key);
                        current = inspect::PointerExtractor<NodePtr>::extract(next);
                    }
                }

                assert(current != nullptr);
                if (current->contains(m_terminator) == true) {
                    // sequence already present, returning
                    return;
                }

                auto newNode = Factory::template make<NodePtr, Terminator>();
                current->add( std::move(newNode) );
            }
            virtual void visitContainer(rct::NodeContainer<TC>&) override { }
            virtual void visitTerminator(rct::NodeTerminator<TC>&) override { }
            virtual ~VisitorRCTInsertSequence() = default;

            const SequenceContainer& sequence() const { return m_sequence; }
            void setSequence(const SequenceContainer& s) { m_sequence = s; }
            const SequenceElement& terminator() const { return m_terminator; }
            void setTerminator(const SequenceElement& s) { m_terminator = s; }

        private:
            SequenceContainer m_sequence;
            SequenceElement m_terminator = Defaults::symTerminator;
            SequenceElement m_wcSingle = Defaults::symWildcardSingle;
    };
}


#endif // __VTRIE_RCT_H__
