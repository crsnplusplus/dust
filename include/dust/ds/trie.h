#ifndef __TRIE_H__
#define __TRIE_H__

#include <iostream>
#include <unordered_map>
#include <stack>
#include <deque>
#include <memory>
#include <utility>
//#include <cassert>

#include <typeinfo>
#include <type_traits>


///////////////////////////////////////////////////////////////////////////////
// inspection tools for tries. c++20 concepts?
namespace dust::trie::inspect {
    // is mappish :) https://stackoverflow.com/questions/35293470/checking-if-a-type-is-a-map
    template<typename T, typename U = void> struct IsMapCompatible : std::false_type { };
    template<typename T>
    struct IsMapCompatible<T, std::void_t<
          typename T::key_type
        , typename T::mapped_type
        , typename T::size_type
        , decltype( std::declval<T&>()[ std::declval<const typename T::key_type&>() ]) // operator[]
        , decltype( std::declval<T&>().at( std::declval<const typename T::key_type&>() )) // .at
        , decltype( std::declval<T&>().contains( std::declval<const typename T::key_type&>() ))
        //, decltype( std::declval<T&>().find( std::declval<const typename T::key_type&>() ))
        , decltype( std::declval<T&>().size())
        , decltype( std::declval<T&>().try_emplace(
            std::declval<const typename T::key_type&>(),
            std::declval<typename T::mapped_type&>() ))
    > > : std::true_type { };

    template<typename T, typename U = void> struct IsFancyPointer : std::false_type { };
    template<typename T> // is fancy pointer
    struct IsFancyPointer<T, std::void_t<
          typename T::element_type
        , decltype( std::declval<T&>().reset() )
        , decltype( std::declval<T&>().get() )
        , decltype( std::declval<T&>().operator->() ) // operator->
        , decltype( std::declval<T&>().operator*() ) // operator*
    > > : std::true_type { };

    template<typename T, typename U = void> struct IsUniquePointer : std::false_type { };
    template<typename T> // is unique pointer
    struct IsUniquePointer<T, std::void_t<
          typename T::pointer
        , typename T::deleter_type
        , decltype( std::declval<T&>().get_deleter() )
    > > : IsFancyPointer<T> { };

    template<typename T, typename U = void> struct IsSharedPointer : std::false_type { };
    template<typename T> // is shared pointer
    struct IsSharedPointer<T, std::void_t<
          typename T::weak_type
        , decltype( std::declval<T&>().use_count() )
        , decltype( std::declval<T&>().swap( std::declval<T&>() ) )
    > > : IsFancyPointer<T> { };

    // is raw pointer
    template<typename T, typename U = void> struct IsRawPointer : std::false_type { };
    template<typename T>
    struct IsRawPointer<T,
        std::enable_if_t<std::is_pointer<T>::value && IsFancyPointer<T>::value == false>
    > : std::true_type { };

    // is pointer
    template<typename T, typename U = void> struct IsSomePointer : std::false_type { };
    template<typename T>
    struct IsSomePointer<T,
        std::enable_if_t<IsRawPointer<T>::value || IsFancyPointer<T>::value>
    > : std::true_type { };

    // concept FancyPointer
    template<typename P>
    concept FancyPointer = requires(P t) { IsFancyPointer<P>::value == true; };

    // concept RawPointer
    template<typename P>
    concept RawPointer = requires(P t) { IsRawPointer<P>::value == true; };

    // concept SomePointer
    template<typename P>
    concept SomePointer = requires(P t) { IsSomePointer<P>::value == true; };

    ///////////////////////////////////////////////////////////////////////////
    // trie config (TC) validators
    template<typename TC, typename U = void> struct IsTrieConfigValid : std::false_type { };
    template<typename TC>
    struct IsTrieConfigValid<TC, std::void_t<
          typename TC::Key
        , typename TC::Value
        , typename TC::ContainerType
    > > : std::true_type { };

    template<typename ConfigKey, typename U = void> struct IsTrieConfigKeyValid : std::false_type { };
    template<typename ConfigKey>
    struct IsTrieConfigKeyValid<ConfigKey, std::enable_if_t<
        std::is_integral<ConfigKey>::value // hashable instead?
    >> : std::true_type { };

    template<typename ConfigValue, typename U = void> struct IsTrieConfigValueValid : std::false_type { };
    template<typename ConfigValue>
    struct IsTrieConfigValueValid<ConfigValue, std::enable_if_t<
        IsFancyPointer<ConfigValue>::value || IsRawPointer<ConfigValue>::value
    >> : std::true_type { };

    template<typename ConfigContainer, typename U = void> struct IsTrieConfigContainerValid : std::false_type { };
    template<typename ConfigContainer>
    struct IsTrieConfigContainerValid<ConfigContainer, std::enable_if_t<
        IsMapCompatible<ConfigContainer>::value
    >> : std::true_type { };

    template<typename TC, typename U = void> struct IsTrieConfigWithUserdata : std::false_type { };
    template<typename TC> // Config (TC) has userdata?
    struct IsTrieConfigWithUserdata<TC, std::void_t< typename TC::Userdata > > : std::true_type { };

    template<typename TC, typename U = void> struct IsTrieConfigWithFactory : std::false_type { };
    template<typename TC> // Config (TC) has factory?
    struct IsTrieConfigWithFactory<TC, std::void_t< typename TC::Factory > > : std::true_type { };

    template<typename TC>
    constexpr bool IsConfigValid() {
        using std::is_class, std::is_union, std::is_same, std::is_same_v;
        static_assert(is_class<TC>::value || is_union<TC>::value, "TC must be a structure or a class (unions?)");
        static_assert(IsTrieConfigValid<TC>::value == true, "Config structure must define the types: TC::Key, TC::Value and TC::ContainerType");
        static_assert(IsTrieConfigKeyValid<typename TC::Key>::value == true, "Key must be integral");
        static_assert(IsTrieConfigValueValid<typename TC::Value>::value == true, "Value must be pointer or smart pointer");
        static_assert(IsTrieConfigContainerValid<typename TC::ContainerType>::value == true, "Container must be map compatible");
        static_assert(is_same_v<typename TC::Key, typename TC::ContainerType::key_type>, "Container Key must be TC::Key");
        static_assert(is_same_v<typename TC::Value, typename TC::ContainerType::mapped_type>, "Container Value must be TC::Value");
        return (is_class<TC>::value || is_union<TC>::value) == true &&
               IsTrieConfigValid<TC>::value == true &&
               IsTrieConfigKeyValid<typename TC::Key>::value == true &&
               IsTrieConfigValueValid<typename TC::Value>::value == true &&
               IsTrieConfigContainerValid<typename TC::ContainerType>::value == true &&
               is_same<typename TC::Key, typename TC::ContainerType::key_type>::value &&
               is_same<typename TC::Value, typename TC::ContainerType::mapped_type>::value;
    }


    ///////////////////////////////////////////////////////////////////////////
    // smart pointer/raw pointer extractors, for uniform interface
    struct RawPointerExtractor  { static constexpr auto extract(auto ptr) { return ptr; } };
    struct FancyPointerExtractor { static constexpr auto extract(auto& fancy) { return fancy.get(); } };
    template<class NodePtr> // Pointer Extractor
    using PointerExtractorType = typename std::conditional<
          inspect::IsFancyPointer<NodePtr>::value
        , FancyPointerExtractor
        , RawPointerExtractor
        >::type;

    template <typename NodePtr>
    struct PointerExtractor : PointerExtractorType<NodePtr> {
        using PointerExtractorType<NodePtr>::extract;
        using ElementType = typename std::pointer_traits<NodePtr>::element_type;
    };
    
}


///////////////////////////////////////////////////////////////////////////////
// factory methods used to build nodes
// support raw, std::unique_ptr and std::shared_ptr with polymorphic types
namespace dust::trie::factory {

    template <class T>
    auto makeRaw(const auto... args) { return std::move(new T(args...)); }
    template <class Base, class Derived=Base, class ... Args>
    auto makeUnique(const Args... args) { return std::move(std::unique_ptr<Base>(new Derived(args...))); }
    template <class Base, class Derived=Base, class ... Args>
    auto makeShared(const Args... args) { return std::move(std::shared_ptr<Base>(new Derived(args...))); }

    template <class Base, class Derived=Base, class D, class ... Args>
    auto makeUniqueWithDeleter( D d , const Args... args) { // d = std::default_delete<Base>()
        return std::move( std::unique_ptr<Base, D>(new Derived(args...), d) );
    }
    template <class Base, class Derived=Base, class D, class ... Args>
    auto makeSharedWithDeleter(D d , const Args... args) { // d = std::default_delete<Base>()
        return std::move( std::shared_ptr<Base>(new Derived(args...), d) );
    }

    template < class ObjType
             , class BaseType// = ObjType
             , class Allocator = std::allocator<ObjType>
             , class ... Args>
    BaseType* makeRawWithAllocator(Allocator& allocator, const Args... args) {
        ObjType* obj = allocator.allocate(1);
        std::allocator_traits<Allocator>::construct(allocator, obj, args...);
        return obj;
    }
    template < class ObjType, class Allocator >
    void destroyRawWithAllocator(Allocator& allocator, ObjType* obj) {
        std::allocator_traits<Allocator>::deallocate(allocator, obj, 1);
    }

    // WithAllocator version for fancy ptrs uses a std::function (DeleterType) with a lambda to store the deleter
    template < class ObjType
             , class BaseType// = ObjType
             , class Allocator// = std::allocator<ObjType>
             , class ... Args>
    auto makeUniqueWithAllocator(Allocator& allocator, const Args... args) {
        static_assert(std::is_base_of<BaseType, ObjType>::value || std::is_same<BaseType, ObjType>::value,
            "BaseType must be equal to ObjType, or must be a base class for ObjType");
        using DeleterType = std::function<void (BaseType*)>;
        DeleterType d = [&allocator](BaseType* p) {
            std::allocator_traits<Allocator>::deallocate(allocator, static_cast<ObjType*>(p), 1);
        };
        ObjType* obj = allocator.allocate(1);
        std::allocator_traits<Allocator>::construct(allocator, obj, args...);
        return std::move(std::unique_ptr<BaseType, DeleterType>(obj, d));
    }
    
    template < class ObjType
             , class BaseType// = ObjType
             , class Allocator// = std::allocator<ObjType>
             , class ... Args>
    auto makeSharedWithAllocator(Allocator& allocator, const Args... args) {
        static_assert(std::is_base_of<BaseType, ObjType>::value || std::is_same<BaseType, ObjType>::value,
            "BaseType must be equal to ObjType, or must be a base class for ObjType");
        using DeleterType = std::function<void (BaseType*)>;
        DeleterType d = [&allocator](BaseType* p) {
            std::allocator_traits<Allocator>::deallocate(allocator, static_cast<ObjType*>(p), 1);
        };
        ObjType* obj = allocator.allocate(1);
        std::allocator_traits<Allocator>::construct(allocator, obj, args...);
        return std::move(std::shared_ptr<BaseType>(obj, d));
    }

    template < class NodePtr // ->Element
             , class Derived = typename std::pointer_traits<NodePtr>::element_type >
    struct Factory {
        template<class ... Args>
        [[nodiscard]] static auto make(const Args... args) {
            if constexpr(inspect::IsRawPointer<NodePtr>::value == true) {
                return std::move( makeRaw<Derived, Args...>(args...) );
            }
            else if constexpr(inspect::IsUniquePointer<NodePtr>::value == true) {
                using Base = typename std::pointer_traits<NodePtr>::element_type;
                return std::move( makeUnique<Base, Derived, Args...>(args...) );
            }
            else if constexpr(inspect::IsSharedPointer<NodePtr>::value == true) {
              using Base = typename std::pointer_traits<NodePtr>::element_type;
                return std::move( makeShared<Base, Derived, Args...>(args...) );
            }
            std::cout << "trie was not here" << std::endl;
            assert(false);
        }
    };

    template < class NodePtr
             , class Base = typename std::pointer_traits<NodePtr>::element_type
             , class Deleter = std::default_delete<Base> >
    struct FactoryWithDeleter {
        template < class ... Args>
        static auto make(Deleter& d, const Args... args) {
            using Element = typename std::pointer_traits<NodePtr>::element_type;
            if constexpr(inspect::IsRawPointer<NodePtr>::value == true) {
                return nullptr;
            }
            else if constexpr(inspect::IsUniquePointer<NodePtr>::value == true) {
                return makeUniqueWithDeleter<Element, Base, Deleter, Args...>(d, args...);
            }
            else if constexpr(inspect::IsSharedPointer<NodePtr>::value == true) {
                return makeSharedWithDeleter<Element, Base, Deleter, Args...>(d, args...);
            }

            //assert(false);
        }
     };

    template < class NodePtr
             , class Base = typename std::pointer_traits<NodePtr>::element_type
             , class Allocator = std::allocator<Base> >
    struct FactoryWithAllocator {
        template < class ... Args>
        static auto make(Allocator& allocator, const Args... args) {
            using Element = typename std::pointer_traits<NodePtr>::element_type;
            if constexpr(inspect::IsRawPointer<NodePtr>::value == true) {
                return makeRawWithAllocator<Element, Base, Allocator, Args...>(allocator, args...);
            }
            else if constexpr(inspect::IsUniquePointer<NodePtr>::value == true) {
                return makeUniqueWithAllocator<Element, Base, Allocator, Args...>(allocator, args...);
            }
            else if constexpr(inspect::IsSharedPointer<NodePtr>::value == true) {
                return makeSharedWithAllocator<Element, Base, Allocator, Args...>(allocator, args...);
            }

            //assert(false);
        }
     };
}



///////////////////////////////////////////////////////////////////////////////
// Skills for tries, implemented as mixins
namespace dust::trie::skills {
    template<typename NodeType>
    struct WithParent { // parent is the owner of *this.
        // members
        NodeType* m_parent = nullptr;
        // interface
        NodeType* parent() const { return m_parent; }
        void setParent(NodeType* p) { m_parent = p; }
        bool hasParent() const { return m_parent == nullptr; }
    };

    template<typename TC>
    struct WithKey {
        using Key = typename TC::Key;
        // members
        Key m_key;
        // interface
        Key key() const { return m_key; }
        void setKey(Key key) { m_key = key; }
    };

    template<class TC>
    struct WithContainer {
        static_assert(inspect::IsMapCompatible<typename TC::ContainerType>());
        using Container = typename TC::ContainerType;
        using Size = typename Container::size_type;
        using Key = typename Container::key_type;
        using Value = typename Container::mapped_type;
        using RawNode = typename std::pointer_traits<Value>::element_type;
        using PE = inspect::PointerExtractor<Value>;
        // members
        Container m_container;
        // interface
        auto add(Key key, Value&& node) {
            const auto n = m_container.try_emplace(key, std::move(node));
            return PE::extract(n.first->second);
        }
        void remove(const Key& key) { m_container.erase(key); }

        Container& container() { return m_container; }
        const Container& container() const { return m_container; }
        
        Value& get(Key key) { return m_container.at(key); }
        const Value& get(Key key) const { return PE::extract(m_container.at(key)); }
        bool contains(const Key key) const { return m_container.contains(key); }
        Size count() const { return m_container.size(); }

        auto getKeys() const {
            std::vector<Key> keys;
            for (const auto& [key, ignored] : m_container) {
                keys.push_back(key);
            }
            return std::move(keys);
        }

        auto getChildren() const {
            std::vector<RawNode*> nodes;
            for (const auto& [ignored, nodePtr] : m_container) {
                RawNode* n = inspect::PointerExtractor<Value>::extract(nodePtr);
                nodes.push_back(n);
            }
            return std::move(nodes);
        }
    };

    template<typename TC>
    struct WithUserdataMember {
        using UserdataPtr = typename TC::Userdata;
        // members
        UserdataPtr m_userdata;
        // interface
        UserdataPtr userdata() { return m_userdata; }
        const UserdataPtr userdata() const { return m_userdata; }
        void setUserdata(const UserdataPtr u) { m_userdata = u; }
    };

    template<>
    struct WithUserdataMember<void*> {
        const void* userdata() const { return 0; }
        void setUserdata(const void*) { }
    };

    template<typename TC>
    struct WithUserdata : std::conditional< inspect::IsTrieConfigWithUserdata<TC>::value
                                          , WithUserdataMember<TC>
                                          , WithUserdataMember<void*>
                          > { };
};

///////////////////////////////////////////////////////////////////////////////
// trie with simple recursive node
namespace dust::trie {

    template<typename TC>
    struct TrieNode : private skills::WithParent<TrieNode<TC>>
                    , private skills::WithContainer<TC>
                    , private skills::WithKey<TC> {
        using Key = typename TC::Key;
        using Value = typename TC::Value;
        using Container = typename TC::ContainerType;
        using NodePtr = typename TC::Value;
        using ParentType = TrieNode<TC>;

        TrieNode() = default;
        TrieNode(const TrieNode& other) = default;  // copy constructor
        TrieNode(TrieNode&& other) = default;       // move constructor
        TrieNode& operator=(const TrieNode& other) { // copy assignment
          return *this = TrieNode(other);
        }
        TrieNode& operator=(TrieNode&& other) noexcept { // move assignment
            std::swap(skills::WithParent<ParentType>::m_parent, other.m_parent);
            std::swap(skills::WithContainer<TC>::m_container, other.m_container);
            std::swap(skills::WithKey<TC>::m_key, other.m_key);
            return *this;
        }
        explicit TrieNode(Key key, ParentType* parent = nullptr)
            : skills::WithParent<ParentType>{parent}
            , skills::WithContainer<TC>{ }
            , skills::WithKey<TC>{key}
            { }
        ~TrieNode() { }

        ParentType* parent() const { return skills::WithParent<ParentType>::parent(); }
        bool hasParent() const { return skills::WithParent<ParentType>::hasParent(); }

        Key key() const { return skills::WithKey<TC>::key(); }
        void setKey(Key key) { skills::WithKey<TC>::setKey(key); }

        auto add(NodePtr&& n) {
            auto key = n->key();
            n->setParent(this);
            return skills::WithContainer<TC>::add(key, std::move(n));
        }
        void remove(const Key& key) { skills::WithContainer<TC>::remove(key); }
        Container& container() { return skills::WithContainer<TC>::container(); }
        const Container& container() const { return skills::WithContainer<TC>::container(); }
        Value& get(Key key) { return skills::WithContainer<TC>::get(key); }
        const Value& get(Key key) const { return skills::WithContainer<TC>::get(key); }
        bool contains(const Key key) const { return skills::WithContainer<TC>::contains(key); }
        auto count() const { return skills::WithContainer<TC>::count(); }
        auto getKeys() const { return std::move(skills::WithContainer<TC>::getKeys()); }
        auto getChildren() const { return std::move(skills::WithContainer<TC>::getChildren()); }

        template<typename Visitor>
        void accept(Visitor& v) {
            v.visit(*this);
        }
    };

    struct DefaultConfig { // TC
        using Key = uint16_t;
        using Value = std::unique_ptr<TrieNode<DefaultConfig>>;
        using ContainerType = std::unordered_map<Key, Value>;
    };
};


///////////////////////////////////////////////////////////////////////////////
// visitors for trie
namespace dust::trie::visitors {
  
    struct Defaults {
        static constexpr auto symTerminator = '#';
        static constexpr auto symWildcardSingle = '.';
    };

    template<class Visitor, class NodeRaw>
    void traverseDFS(Visitor& visitor, NodeRaw* root) {
        using DiscoveryStack = std::stack<NodeRaw*>;
        assert(root != nullptr);

        DiscoveryStack stack;
        NodeRaw* current = root;
        visitor.visitRoot(current);

        for (auto child : root->getChildren())
            stack.push(child);

        while(stack.empty() == false) {
            current = stack.top();
            stack.pop();

            if (current->count() > 0)
                visitor.visitContainer(current);
            else
                visitor.visitTerminator(current);

            auto children = current->getChildren();
            for (auto child : children) {
                stack.push(child);
            }
        }
    };


    template<class Visitor, class NodeRaw>
    void traverseBFS(Visitor& visitor, NodeRaw* root) {
        using DiscoveryQueue = std::deque<NodeRaw*>;

        DiscoveryQueue queue;
        NodeRaw* current = root;
        visitor.visitRoot(current);

        auto rootChildren = root->getChildren();
        for (auto& child : rootChildren) {
            queue.push_back(child);
        }

        // visiting children
        while(queue.empty() == false) {
            current = queue.front();
            queue.pop_front();

            if (current->count() > 0)
                visitor.visitContainer(current);
            else
                visitor.visitTerminator(current);

            auto children = current->getChildren();
            for (auto& child : children) {
                queue.push_back(child);
            }
        }
    };

    struct StrategyDFS {};
    struct StrategyBFS {};

    template <class TraverseStrategy, class Visitor, class NodeRaw>
    auto visitWithStrategy(Visitor& v, NodeRaw* node) {
        if constexpr(std::is_same_v<TraverseStrategy, StrategyDFS>) {
            return traverseDFS<Visitor, NodeRaw>(v, node);
        }
        else if constexpr(std::is_same_v<TraverseStrategy, StrategyBFS>) {
            return traverseBFS<Visitor, NodeRaw>(v, node);
        }
    }

    template< class TC
            , class TraverseStrategy=StrategyDFS
            >
    class VisitorCollectAllSequences {
        public:
            using NodePtr = typename TC::Value;
            using Key = typename TC::Key;
            using NodeRaw = typename std::pointer_traits<NodePtr>::element_type;
            using TerminatorsVector = std::vector<NodeRaw*>;
            void visitRoot(NodeRaw* r) { }
            void visitContainer(NodeRaw*) { }
            void visitTerminator(NodeRaw* t) { m_terminators.push_back(t); }

            auto visit(NodeRaw& r) { return visitWithStrategy<TraverseStrategy>(*this, &r); }
            const auto& collectedTerminators() const { return m_terminators; }
            void reset() { m_terminators = TerminatorsVector(); }

        private:
            TerminatorsVector m_terminators;
    };


    template<class Visitor, class NodeRaw>
    void traverseIterative(Visitor& visitor, NodeRaw* root) {
        using DiscoveryStack = typename Visitor::DiscoveryStack;
        if (root == nullptr)
            return;

        NodeRaw* current = root;
        visitor.visitRoot(current);
        DiscoveryStack stack;

        while(visitor.hasStack() == true) {
            visitor.getNext(stack);

            while(stack.empty() == false) {
                current = stack.top();
                stack.pop();
                
                if (current->count() > 0)
                    visitor.visitContainer(current);
                else
                    visitor.visitTerminator(current);
            }
        }
    };

    template<class TC, class Pattern = std::string>
    class VisitorFindPattern {
        public:
            using NodePtr = typename TC::Value;
            using PE = inspect::PointerExtractor<NodePtr>;
            using NodeRaw = typename std::pointer_traits<NodePtr>::element_type;
            //using NodeRaw = PE<NodePtr>::element_type;
            using Key = typename TC::Key;
            
            using Size = typename Pattern::size_type;
            using Sequence = Pattern;
            using DiscoveryStack = std::stack<NodeRaw*>;
            using TerminatorsVector = std::vector<NodeRaw*>;
            using FindResults = std::vector<Sequence>;

            void visitRoot(NodeRaw* node) { visitContainer(node); }
            void visitContainer(NodeRaw* node) {
                assert (m_position <= m_pattern.size());

                if (m_position == m_pattern.size()) {
                    if (node->contains(m_terminator)) {
                        // found
                        m_stack.push(PE::extract(node->get(m_terminator)));
                        return;
                    }
                    return;
                }

                const Key key = m_pattern[m_position];
                if (key == m_wildcard) {
                    auto children = node->getChildren();
                    for (auto child : children) {
                        m_stack.push(child);
                    }
                }
                else if (node->contains(key)) {
                    m_stack.push(PE::extract(node->get(key)));
                }
            }

            void visitTerminator(NodeRaw* t) {
                if (m_position != m_pattern.size() + 1)
                    return;

                assert(t->key() == m_terminator);
                m_collected.push_back(t);
            }

            auto visit(NodeRaw& node) {
                m_position = 0;
                m_stack = DiscoveryStack();
                m_collected.clear();
                m_collected.shrink_to_fit();
                traverseIterative(*this, &node);
            }

            bool hasStack() const { return m_stack.empty() == false; }
            void getNext(DiscoveryStack& s) {
                s = std::move(m_stack);
                m_stack = std::move(DiscoveryStack());
                ++m_position;
            }
    
            FindResults getResults() const {
                //std::cout << "  + getting results: " << std::endl;
                const auto& terminators = collected();
                FindResults results;

                for (const auto& t : terminators) {
                    auto current = t->parent();
                    assert(current != nullptr);
                    
                    Sequence sequence;
                    while (current->parent() != nullptr) {
                        sequence.insert(sequence.begin(), current->key());
                        current = current->parent();
                    }
                    results.push_back(sequence);
                    //std::cout << "    -" << sequence << std::endl;
                }

                return std::move(results);
            }

            const Pattern& pattern() const { return m_pattern; }
            void setPattern(const Pattern& p) { m_pattern = p; }  
            const Key& terminator() const { return m_terminator; }
            void setTerminator(const Key& s) { m_terminator = s; }
            void wildcard(const Key& s) { return m_wildcard; }
            void setWildcard(const Key& s) { m_wildcard = s; }
            auto& collected() const { return m_collected; }
            void clearCollected() { m_collected.clear(); }

        private:
            Pattern m_pattern;
            Size m_position = 0;
            DiscoveryStack m_stack;

            TerminatorsVector m_collected;
            Key m_terminator = Defaults::symTerminator;
            Key m_wildcard = Defaults::symWildcardSingle;
    };

    template<class NodePtr, class Factory, class SequenceContainer>
    class VisitorInsertSequence {
            using NodeRaw = typename std::pointer_traits<NodePtr>::element_type;
            using SequenceElement = typename SequenceContainer::value_type;
        public:
            VisitorInsertSequence() = default;
            VisitorInsertSequence(const SequenceElement terminator) : m_terminator(terminator){}

            void visit(NodeRaw& n) {
                NodeRaw* current = &n;

                for (auto& s : m_sequence) {
                    assert(s != m_terminator);
                    if (current->contains(s) == true) {
                        assert(current->get(s) != nullptr);
                        current = inspect::PointerExtractor<NodePtr>::extract(current->get(s));
                        continue;
                    }
                    current = current->add( std::move(Factory::make(s, current)) );
                }

                if (current->contains(m_terminator) == true)
                    // sequence was already present
                    return;

                current->add( std::move(Factory::make(m_terminator, current)) );
            }

            const SequenceContainer& sequence() const { return m_sequence; }
            void setSequence(const SequenceContainer& s) { m_sequence = s; }
            const SequenceElement& terminator() const { return m_terminator; }
            void setTerminator(const SequenceElement& s) { m_terminator = s; }

        private:
            SequenceContainer m_sequence;
            SequenceElement m_terminator = Defaults::symTerminator;
    };

    template<class NodePtr, class SequenceContainer>
    class VisitorRemoveSequence {
            using NodeRaw = typename std::pointer_traits<NodePtr>::element_type;
            using SequenceElement = typename SequenceContainer::value_type;
        public:
            VisitorRemoveSequence() = default;
            VisitorRemoveSequence(const SequenceElement terminator) : m_terminator(terminator){}

            void visit(NodeRaw& n) {
                if (std::size(m_sequence) == 0)
                    return;
                
                NodeRaw* start = &n;
                NodeRaw* current = start;

                for (auto& s : m_sequence) {

                    if (current->contains(s) == false)
                        return; // sequence incomplete, exiting

                    NodePtr& next = current->get(s);
                    current = inspect::PointerExtractor<NodePtr>::extract(next);
                }

                assert(current != start);
                if (current->contains(m_terminator) == false) {
                    // doesn't contain a terminator, exiting
                    return;
                }

                // removing terminator
                assert(current->contains(m_terminator));
                current->remove(m_terminator);
                assert(current->contains(m_terminator) == false);

                auto ritStart = m_sequence.rbegin();

                while (current != start) {
                    assert(ritStart != m_sequence.rend());
                    auto key = *ritStart++;
                    current = current->parent();

                    if (current->count() > 1)
                        // will delete only this sequence
                        break;

                    assert(current->contains(key));
                    current->remove(key);
                }
            }

            const SequenceContainer& sequence() const { return m_sequence; }
            void setSequence(const SequenceContainer& s) { m_sequence = s; }
            const SequenceElement& terminator() const { return m_terminator; }
            void setTerminator(const SequenceElement& s) { m_terminator = s; }

        private:
            SequenceContainer m_sequence;
            SequenceElement m_terminator;
    };


    template<class NodePtr, class _, class SequenceContainer=std::string>
    class VisitorFindSequence {
            using NodeRaw = typename std::pointer_traits<NodePtr>::element_type;
            using SequenceElement = typename SequenceContainer::value_type;
            using PE = inspect::PointerExtractor<NodePtr>;
        public:
            VisitorFindSequence() = default;
            VisitorFindSequence(const SequenceElement terminator) : m_terminator(terminator){}

            void visit(NodeRaw& n) {
                m_found = false;
                NodeRaw* current = &n;

                for (auto& s : m_sequence) {
                    assert(s != m_terminator);
                    assert(current != nullptr);
                    if (current->contains(s) == true) {
                        assert(current->get(s) != nullptr);
                        current = PE::extract(current->get(s));
                        continue;
                    }
                    else if(s == m_wcSingle && current->count() > 0) {
                        // user enterd a wildcard, so we should accept
                        // every value, but only if the current node has
                        // children.
                        continue;
                    }
                    // not contained, returning
                    return;
                }

                assert(current != nullptr);
                m_found = current->contains(m_terminator);
            }

            const SequenceContainer& sequence() const { return m_sequence; }
            void setSequence(const SequenceContainer& s) { m_sequence = s; }
            const SequenceElement& terminator() const { return m_terminator; }
            void setTerminator(const SequenceElement& s) { m_terminator = s; }
            SequenceElement wildcardSingle() const { return m_wcSingle; }
            void setWildcardSingle(const SequenceElement s) { m_wcSingle = s; }
            bool found() const { return m_found; }

        private:
            SequenceContainer m_sequence;
            SequenceElement m_terminator = Defaults::symTerminator;
            SequenceElement m_wcSingle = Defaults::symWildcardSingle;
            bool m_found = false;
    };

    template<class NodePtr, class SequenceContainer>
    class VisitorRetrievePattern {
            using NodeRaw = typename std::pointer_traits<NodePtr>::element_type;
            using SequenceElement = typename SequenceContainer::value_type;
            using PE = inspect::PointerExtractor<NodePtr>;
            using DiscoveryStack = std::stack<NodeRaw*>;
            using DiscoveredMap = std::unordered_map<NodeRaw*, bool>;
        public:
            VisitorRetrievePattern() = default;
            VisitorRetrievePattern(const SequenceElement terminator) : m_terminator(terminator){ }

            void visit(NodeRaw& root) {
                std::unordered_map<NodeRaw*, bool> discovered;
                DiscoveryStack stack;

                stack.push(&root);

                while (stack.empty() == false) {
                    NodeRaw* node = stack.top();
                    stack.pop();

                    if (discovered[node] == true) {
                        assert(false);
                        continue;
                    }
                    
                    discovered[node] = true;

                    auto keys = node->getKeys();
                    auto children = node->getChildren();

                    for (auto child : children) {
                        if (discovered[child] == true)
                            continue;

                        stack.push(child);
                    }

                    if (node->key() == m_terminator) {
                        NodeRaw* current = node;
                        std::vector<SequenceElement> reversed;
                        while (current->parent() != nullptr) {
                            reversed.push_back(current->key());
                            current = current->parent();
                        }
                        //for (auto rit = reversed.rbegin(), rend = reversed.rend(); rit!= rend; ++rit) {
                        //  std::cout << *rit;
                        //}
                        //std::cout << std::endl;
                    }
                }
            }

            const SequenceContainer& sequence() const { return m_sequence; }
            void setSequence(const SequenceContainer& s) { m_sequence = s; }
            const SequenceElement& terminator() const { return m_terminator; }
            void setTerminator(const SequenceElement& s) { m_terminator = s; }
            SequenceElement wildcardSingle() const { return m_wcSingle; }
            void setWildcardSingle(const SequenceElement s) { m_wcSingle = s; }

        private:
            SequenceContainer m_sequence;
            SequenceElement m_terminator = Defaults::symTerminator;
            SequenceElement m_wcSingle = Defaults::symWildcardSingle;
    };

    template<typename Lambda>
    class VisitorLambda {
        public:
            VisitorLambda() = default;
            VisitorLambda(Lambda& l) : m_lambda(l) {}
            void visit(auto& n) {
                m_lambda(n);
            }
        private:
            Lambda m_lambda;
    };
}


#endif // __TRIE_H__
