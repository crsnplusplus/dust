#ifndef __GRAPH_VISITOR_H__
#define __GRAPH_VISITOR_H__

#include <dust/ds/graph.h>
#include <iostream>
#include <sstream>
#include <stack>
#include <list>

namespace dust::ds {

    template <typename GT>
    class graph_visitor_dfs : public graph_vertex_visitor_base<GT> {
    public:
        using vindex = typename GT::vindex;
        using vertex_ptr = typename GT::vertex_ptr;
        using vertex_list = std::vector<vertex_ptr>;
        using vmap = typename GT::vmap;
        using vdiscovery_stack = std::stack<vertex_ptr>;
        using vdiscovery_map = std::unordered_map<vertex_ptr, bool>;

        virtual ~graph_visitor_dfs() { }

        vindex start_vertex_index() const { return m_vindex_start; }
        void set_start_vertex_index(vindex vidx) { m_vindex_start = vidx; }
        virtual void visit(graph<GT>& g) override {
            if (g.vertices_count() == 0)
                return;

            for (auto vmap_entry : g.vertices_map())
                m_discovered[vmap_entry.first] = false;

            vdiscovery_stack stack;
            //vertex_ptr vp_start = g.vertex_at(m_vindex_start);
            vertex_ptr vp_start = g.find_by_index(m_vindex_start);
            if (vp_start == nullptr) {
                // cannot start from this vertex, fallback
                const vmap& map = g.vertices_map();
                vp_start = map.begin()->first;
            }

            assert(vp_start != nullptr);
            stack.push(vp_start);

            while(stack.empty() == false) {
                vertex_ptr vp = stack.top();
                stack.pop();

                if (m_discovered[vp] == true)
                    continue;

                visit(vp);

                for(auto e : g.edges(vp)) {
                    assert(e != nullptr);
                    if (m_discovered[e] == true)
                        continue;

                    stack.push(e);
                }
            }
        }

        virtual void visit(vertex_ptr v) override {
            assert(v != nullptr);
            assert (m_discovered[v] == false);

            m_discovered[v] = true;
            m_list.push_back(v);
        }

        vertex_list order() { return m_list; }

        private:
            vindex m_vindex_start;
            vertex_list m_list;
            vdiscovery_map m_discovered;
    };

    template <typename GT>
    class graph_visitor_bfs : public graph_vertex_visitor_base<GT> {
        public:
            using vindex = typename GT::vindex;
            using vertex_ptr = typename GT::vertex_ptr;
            using vertex_list = std::vector<vertex_ptr>;
            using vmap = typename GT::vmap;
            using vdiscovery_queue = std::list<vertex_ptr>;
            using vdiscovery_map = std::unordered_map<vertex_ptr, bool>;
            virtual ~graph_visitor_bfs() { }

            vindex start_vertex_index() const { return m_vindex_start; }
            void set_start_vertex_index(vindex vidx) { m_vindex_start = vidx; }
            virtual void visit(graph<GT>& g) override {
                if (g.vertices_count() == 0)
                    return;

                for (auto vmap_entry : g.vertices_map())
                    m_discovered[vmap_entry.first] = false;

                vdiscovery_queue queue;
                //vertex_ptr vp_start = g.vertex_at(m_vindex_start);
                vertex_ptr vp_start = g.find_by_index(m_vindex_start);
                if (vp_start == nullptr) {
                    // cannot start from this vertex, fallback
                    const vmap& map = g.vertices_map();
                    vp_start = map.begin()->first;
                }

                assert(vp_start != nullptr);
                queue.push_back(vp_start);

                while(queue.empty() == false) {
                    vertex_ptr vp = queue.front();
                    queue.pop_front();

                    if (m_discovered[vp] == true)
                        continue;

                    visit(vp);

                    for(auto e : g.edges(vp)) {
                        assert(e != nullptr);
                        if (m_discovered[e] == true)
                            continue;

                        queue.push_back(e);
                    }
                }
            }

            virtual void visit(vertex_ptr v) override {
                assert(v != nullptr);
                assert (m_discovered[v] == false);

                m_discovered[v] = true;
                m_list.push_back(v);
            }

            vertex_list order() { return m_list; }
        private:
            vindex m_vindex_start;
            vertex_list m_list;
            vdiscovery_map m_discovered;
    };

    template <typename GT>
    class graph_visitor_print : public graph_vertex_visitor_base<GT> {

        public:
            using vertex_ptr = typename GT::vertex_ptr;

            virtual ~graph_visitor_print() { }


            virtual void visit(graph<GT>& g) override {
                if (g.vertices_count() == 0)
                    return;

                typename GT::vertex_ptr vp = g.start_vertex();
                assert(vp != nullptr);
                visit(vp);
            }

            virtual void visit(vertex_ptr vertex) override {
                std::cout << "visiting node " << vertex->index() << " from graph_visitor_print" << std::endl;
            }
    };


    template <typename GT>
    class graph_visitor_print_dot_format : public graph_vertex_visitor_base<GT> {

        public:
            using graph = typename GT::graph;
            using path_type = typename GT::path_type;
            using vindex = typename GT::vindex;
            using emap = typename GT::emap;
            using vertex_ptr = typename GT::vertex_ptr;

            graph_visitor_print_dot_format(const std::string& vprefix = "v",
                                           const std::string& vlabel = "vertex")
                : m_graph(nullptr), m_vprefix(vprefix), m_vlabel(vlabel) { }
            virtual ~graph_visitor_print_dot_format() { }

            virtual void visit(graph& g) override {
                m_graph = &g;
                if (m_graph->vertices_count() == 0)
                    return;

                auto map = m_graph->vertices_map();
                for (auto vpair  : map) {
                    visit(vpair.first);
                }
            }

            virtual void visit(vertex_ptr vertex) override {
                assert(m_graph->has_vertex(vertex));
                emap& list = m_graph->edges(vertex);
                std::ostringstream dot;
                const std::string __ = "  ";

                dot << __
                    << m_vprefix << vertex->index()
                    << " [label=\"" << m_vlabel << vertex->index() << "\"]"
                    << std::endl;

                for (size_t i = 0; i < list.size(); ++i) {
                    const vertex_ptr vp = list[i];
                    assert(vp != nullptr);

                    dot << __
                        << m_vprefix << vertex->index()
                        << " -- "
                        << m_vprefix << vp->index()
                        << std::endl;
                }

                m_out += dot.str();
            }

            void reset () {
                m_out.clear();
            }

            std::string dot_format(const std::string& gname="dust") const {
                std::ostringstream dot;
                dot << "graph " << gname <<  " {" << std::endl
                    << m_out
                    << "}"
                    << std::endl;

                return dot.str();
            }


        private:
            graph* m_graph;
            std::string m_vprefix;
            std::string m_vlabel;
            std::string m_out;
    };
}


#endif // __GRAPH_VISITOR_H__
