#ifndef __TRIE_CONFIGS_H__
#define __TRIE_CONFIGS_H__

#include <dust/ds/trie.h>

#include <list>
#include <map>

#include <memory>
#include <typeinfo>
#include <type_traits>

#include <boost/core/demangle.hpp>
#include <boost/unordered_map.hpp>

#include <tsl/robin_map.h>
#include <tsl/bhopscotch_map.h>


namespace dust::trie::adapters {

    template<typename T>
    class BoostUnorderedMapAdapter : T {
        public:
            using typename T::key_type;
            using typename T::mapped_type;
            using typename T::size_type;
            using T::at;
            using T::operator[];
            using T::find;
            using T::size;
            using T::insert_or_assign;
            using T::try_emplace;
            using T::erase;
            using T::begin;
            using T::cbegin;
            using T::end;
            using T::cend;

            bool contains( const key_type& key ) const {
                return find(key) != cend();
            }
            template< class K >
            bool contains( const K& x ) const {
              return find(x) != cend();
            }
    };
}

namespace dust::trie::config {
    using dust::trie::TrieNode;

    // std::map
    struct RawStdMap {
        using Key = uint8_t;
        using Value = TrieNode<RawStdMap>*;
        using ContainerType = std::unordered_map<Key, Value>;
    };

    struct UniqueStdMap {
        using Key = uint8_t;
        using Value = std::unique_ptr<TrieNode<UniqueStdMap>>;
        using ContainerType = std::unordered_map<Key, Value>;
    };

    struct SharedStdMap {
        using Key = uint8_t;
        using Value = std::shared_ptr<TrieNode<SharedStdMap>>;
        using ContainerType = std::unordered_map<Key, Value>;
    };

    // boost::container::map
    struct RawBoostMap {
        using Key = uint8_t;
        using Value = TrieNode<RawBoostMap>*;
        using ContainerType = dust::trie::adapters::BoostUnorderedMapAdapter<boost::unordered_map<Key, Value>>;
    };

    struct UniqueBoostMap {
        using Key = uint8_t;
        using Value = std::unique_ptr<TrieNode<UniqueBoostMap>>;
        using ContainerType = dust::trie::adapters::BoostUnorderedMapAdapter<boost::unordered_map<Key, Value>>;
    };

    struct SharedBoostMap {
        using Key = uint8_t;
        using Value = std::shared_ptr<TrieNode<SharedBoostMap>>;
        using ContainerType = dust::trie::adapters::BoostUnorderedMapAdapter<boost::unordered_map<Key, Value>>;
    };


    // tsl::robin_map
    struct RawRobinMap {
        using Key = uint8_t;
        using Value = TrieNode<RawRobinMap>*;
        using ContainerType = tsl::robin_map<Key, Value>;
    };

    struct UniqueRobinMap {
        using Key = uint8_t;
        using Value = std::unique_ptr<TrieNode<UniqueRobinMap>>;
        using ContainerType = tsl::robin_map<Key, Value>;
    };

    struct SharedRobinMap {
        using Key = uint8_t;
        using Value = std::shared_ptr<TrieNode<SharedRobinMap>>;
        using ContainerType = tsl::robin_map<Key, Value>;
    };

    // tsl::bhopscotch_map
    struct RawHopscotchMap {
        using Key = uint8_t;
        using Value = TrieNode<RawHopscotchMap>*;
        using ContainerType = tsl::bhopscotch_map<Key, Value>;
    };

    struct UniqueHopscotchMap {
        using Key = uint8_t;
        using Value = std::unique_ptr<TrieNode<UniqueHopscotchMap>>;
        using ContainerType = tsl::bhopscotch_map<Key, Value>;
    };

    struct SharedHopscotchMap {
        using Key = uint8_t;
        using Value = std::shared_ptr<TrieNode<SharedHopscotchMap>>;
        using ContainerType = tsl::bhopscotch_map<Key, Value>;
    };


}


#endif // __TRIE_CONFIGS_H__
