#ifndef __TRIE_PROFILES_H__
#define __TRIE_PROFILES_H__

#include "trie_configs.h"

namespace dust::trie::profiles {


template<class TC = dust::trie::config::UniqueStdMap>
class Trie {
    public:
        using Config = TC;
        using NodePtr = typename TC::Value;
        using Factory = dust::trie::factory::Factory<NodePtr>;
        using VisitorStorage = std::string;
        using VisitorInsert = dust::trie::visitors::VisitorInsertSequence<NodePtr, Factory, VisitorStorage>;
        using VisitorRemove = dust::trie::visitors::VisitorRemoveSequence<NodePtr, VisitorStorage>;
        using VisitorFind = dust::trie::visitors::VisitorFindPattern<TC, VisitorStorage>;
        using FindResults = std::vector<VisitorStorage>;

    public:
        Trie() : 
            m_root(Factory::make(0, nullptr)),
            m_wildcard(typename TC::Key('.')),
            m_terminator(typename TC::Key('#')) { }

        Trie(typename TC::Key wildcard, typename TC::Key terminator) :
            m_root(Factory::make(0, nullptr)),
            m_wildcard(wildcard),
            m_terminator(terminator) { }
        ~Trie() {}

        void add_word(const std::string& word) {
            //std::cout << "adding word: " << word << std::endl;
            VisitorInsert v;
            v.setTerminator(m_terminator);
            v.setSequence(word);
            m_root->accept(v);
        }

        void remove_word(const std::string& word) {
            //std::cout << "removing word: " << word << std::endl;
            VisitorRemove v;
            v.setTerminator(m_terminator);
            v.setSequence(word);
            m_root->accept(v);
        }

        FindResults find(const std::string& pattern) {
            //std::cout << "finding pattern: " << pattern << std::endl;

            VisitorFind v;
            v.setWildcard(m_wildcard);
            v.setTerminator(m_terminator);
            v.setPattern(pattern);
            m_root->accept(v);
            return v.getResults();
        }

    private:
        NodePtr m_root;
        typename TC::Key m_wildcard;
        typename TC::Key m_terminator;
};



template<class TC = dust::trie::config::UniqueStdMap>
class TrieWithPagesByLength {
    public:
        using Config = TC;

        using NodePtr = typename TC::Value;
        using Factory = dust::trie::factory::Factory<NodePtr>;
        using PagesContainer = std::unordered_map<uint8_t, NodePtr>;

        using VisitorStorage = std::string;
        using VisitorInsert = dust::trie::visitors::VisitorInsertSequence<NodePtr, Factory, VisitorStorage>;
        using VisitorRemove = dust::trie::visitors::VisitorRemoveSequence<NodePtr, VisitorStorage>;
        using VisitorFind = dust::trie::visitors::VisitorFindPattern<TC, VisitorStorage>;
        using FindResults = std::vector<VisitorStorage>;

    public:
        TrieWithPagesByLength() {
            vRemover.setTerminator(typename TC::Key('#'));
            vInserter.setTerminator(typename TC::Key('#'));
            vFinder.setTerminator(typename TC::Key('#'));        
            vFinder.setWildcard(typename TC::Key('.'));
        }

        TrieWithPagesByLength(typename TC::Key wildcard, typename TC::Key terminator) {
            vInserter.setTerminator(terminator);
            vRemover.setTerminator(terminator);
            vFinder.setWildcard(wildcard);
            vFinder.setTerminator(terminator);
        }
        ~TrieWithPagesByLength() { }

        void add_word(const std::string& word) {
            //std::cout << "adding word: " << word << std::endl;
            vInserter.setSequence(word);

            const size_t len = word.length();
            if (pages.contains(len) == false) {
                pages[len] = Factory::make(0, nullptr);
            }

            const auto& page_node = pages[len];
            page_node->accept(vInserter);
        }

        bool has_page(typename PagesContainer::key_type page_num) const {
            return pages.contains(page_num);
        }

        void remove_word(const std::string& sequence) {
            //std::cout << "removing sequence: " << sequence << std::endl;
            const typename PagesContainer::key_type len = sequence.length();
            if (has_page(len) == false)
                return;

            vRemover.setSequence(sequence);
            pages[len]->accept(vRemover);
        }

        FindResults find(const std::string& pattern) {
            //std::cout << "finding pattern: " << pattern << std::endl;
            const typename PagesContainer::key_type len = pattern.length();
            assert(has_page(len));

            vFinder.setPattern(pattern);
            pages[len]->accept(vFinder);
            return vFinder.getResults();
        }

    private:
        VisitorInsert vInserter;
        VisitorRemove vRemover;
        VisitorFind vFinder;

        // pages by lenght
        PagesContainer pages;
};



} // namespace dust::trie::profiles

#endif // __TRIE_PROFILES_H__
