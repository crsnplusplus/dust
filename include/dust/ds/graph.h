#pragma once

#include <map>
#include <unordered_map>
#include <deque>
#include <algorithm>

namespace dust::ds {
    template<typename GT> class graph;
    template<typename GT> class graph_vertex;
    template<typename GT> class graph_vertex_visitor_base;


    template <typename V, // V = vertex index type
              typename U = void*> // U = vertex userdata
    struct graph_traits {
        static_assert(std::is_arithmetic<V>::value, "V (vertex index size type) must be numeric");
        static_assert(std::is_pointer<U>::value, "U (user data pointer type) must be a pointer");

        using traits = graph_traits<V, U>;
        using vindex = V;
        using vuserdata_ptr = U;
        using vertex = graph_vertex<traits>;
        using vertex_ptr = vertex*;
        using graph = class graph<traits>;

        using emap = std::deque<vertex_ptr>;
        using vmap = std::map<vertex_ptr, emap>;

        using vertex_visitor_interface = graph_vertex_visitor_base<traits>;
        using path_type = std::wstring;
    };


    template <typename GT>
    class graph_vertex_visitor_base {
        public:
            using graph = typename GT::graph;
            using vertex_ptr = typename GT::vertex_ptr;

            virtual void visit(graph& g) = 0;
            virtual void visit(vertex_ptr vp) = 0;
            virtual ~graph_vertex_visitor_base() = default;
    };


    template<typename GT>
    class graph_vertex {

        public:
            using vindex = typename GT::vindex;
            using vuserdata_ptr = typename GT::vuserdata_ptr;
            using graph = typename GT::graph;
            using vertex = typename GT::vertex;
            using vertex_ptr = typename GT::vertex_ptr;

            explicit graph_vertex(vindex index, const vuserdata_ptr ud = nullptr)
                : m_index(index), m_userdata(ud) { }
            virtual ~graph_vertex() = default;

            virtual void accept(graph_vertex_visitor_base<GT>& v) {
                v.visit(this);
            }

            vindex index() const { return m_index; }

            vuserdata_ptr userdata() { return m_userdata; }
            void set_userdata(vuserdata_ptr ud) { m_userdata = ud; }

        private:
            const vindex m_index;
            vuserdata_ptr m_userdata;
    };


    template<typename GT>
    class graph {
        public:
            using vindex = typename GT::vindex;
            using vuserdata_ptr = typename GT::vuserdata_ptr;
            using vertex = typename GT::vertex;
            using vertex_ptr = typename GT::vertex_ptr;
            using vmap = typename GT::vmap;
            using emap = typename GT::emap;

        public:
            graph() { }
            explicit graph(const vmap& m) { m_vmap = m; }
            virtual ~graph() = default;

            vindex vertices_count() const { return m_vmap.size(); }
            const vmap& vertices_map() const { return m_vmap; }
            void add_vertex(vertex_ptr v, emap edges = emap()) { m_vmap[v] = edges; }
            void remove_vertex(vertex_ptr v) {
                if (auto it = m_vmap.find(v); it != m_vmap.end())
                    remove(it);
            }
            bool has_vertex(const vertex_ptr v) {
                return m_vmap.find(v) != m_vmap.end();
            }
            vertex_ptr start_vertex() const {
                if (m_vmap.size() == 0)
                    return nullptr;

                return m_vmap.begin()->first;
            }
            vertex_ptr find_by_index(vindex i) {
                for (auto vpair : m_vmap) {
                    if (vpair.first->index() != i)
                        continue;

                    return vpair.first;
                }
                return nullptr;
            }
            void add_edge(vertex_ptr vp, vertex_ptr edge) {
                m_vmap[vp].push_back(edge);
            }
            bool has_edge(vertex_ptr vp, vertex_ptr edge) {
                assert(has_vertex(vp) == true);
                assert(has_vertex(edge) == true);
                emap& emap_vp = m_vmap[vp];
                return std::find(emap_vp.begin(), emap_vp.end(), edge) != emap_vp.end();
            }
            void remove_edge(vertex_ptr vp, vertex_ptr edge) {
                assert(has_vertex(vp) == true);
                assert(has_vertex(edge) == true);
                emap& emap_vp = m_vmap[vp];
                if (auto it = emap_vp.find(edge); it != emap_vp.end())
                    emap_vp.remove(it);
            }
            emap& edges(vertex_ptr vp) {
                assert(has_vertex(vp) == true);
                return m_vmap[vp];
            }

        private:
            vmap m_vmap;
    };
} // dust::ds
