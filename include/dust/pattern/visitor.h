#pragma once

namespace dust::ds {
// taken from stackoverflow, but also here:
// https://gist.github.com/tomas789/7844152
// thanks, very elegant :)
template<typename... TList> class Visitor;

template<typename T>
class Visitor<T> {
    public:
        virtual void visit(T & visitable) = 0;
};

template<typename T, typename... TList>
class Visitor<T, TList...> : public Visitor<TList...> {
public:
    // promote the function(s) from the base class
    using Visitor<TList...>::visit;
    virtual void visit(T& visitable) = 0;
};

template<typename... TList>
class Visitable {
    public:
        virtual void accept(Visitor<TList...>& visitor) = 0;
};


template <typename Derived, typename ... TList>
class VisitableImpl : public Visitable<TList ...> {
    public:
        virtual void accept(Visitor<TList ...> & visitor) override {
            Derived& d = static_cast<Derived &>(*this);
            visitor.visit(d);
    }
};

}
