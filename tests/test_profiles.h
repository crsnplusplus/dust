#pragma once

#include <dust/ds/trie.h>

#include <list>
#include <map>

#include <memory>
#include <typeinfo>
#include <type_traits>

#include <boost/core/demangle.hpp>
#include <boost/unordered_map.hpp>
#include <boost/container/map.hpp>

#include <frozen/unordered_map.h>
#include <frozen/map.h>

#include <tsl/robin_map.h>
#include <tsl/bhopscotch_map.h>

namespace dust::test {
    using dust::trie::TrieNode;

    struct TypeIDPrinterVisitor {
        template<typename T, bool printVisitor = false, bool showExtended = false, bool showTrivial = false, bool showsNodeInfo = false>
        void visit(const T& n) const {
            using boost::core::demangle;
            if constexpr(printVisitor == true) {
                std::cout << "[" << demangle( typeid(*this).name() ) << "]" << std::endl;
            }

            std::cout << "type is: " << demangle( typeid(n).name() ) << std::endl;

            if constexpr(showExtended == true) {
                std::cout << "  is_integral        " << (std::is_integral<decltype(n)>::value == true ? "yes" : "no") << std::endl;
                std::cout << "  is_arithmetic      " << (std::is_arithmetic<decltype(n)>::value == true ? "yes" : "no") << std::endl;
                std::cout << "  is_pointer         " << (std::is_pointer<decltype(n)>::value == true ? "yes" : "no") << std::endl;
                std::cout << "  is_reference       " << (std::is_reference<decltype(n)>::value == true ? "yes" : "no") << std::endl;
                std::cout << "  is_object          " << (std::is_object<decltype(n)>::value == true ? "yes" : "no") << std::endl;
                std::cout << "  is_standard_layout " << (std::is_standard_layout<decltype(n)>::value == true ? "yes" : "no") << std::endl;
                std::cout << "  is_trivial         " << (std::is_standard_layout<decltype(n)>::value == true ? "yes" : "no") << std::endl;
                std::cout << "  is_pod             " << ( ((std::is_standard_layout<decltype(n)>::value == true) && 
                                                           (std::is_trivial<decltype(n)>::value == true) ? "yes" : "no"))
                                                         << std::endl;
                std::cout << std::endl;
            }
            if constexpr(showTrivial == true) {
                std::cout << "  [Trivial properties]" << std::endl;
                std::cout << "  is_trivial                         " << (std::is_trivial<decltype(n)>::value == true ? "yes" : "no") << std::endl;
                std::cout << "  is_trivially_default_constructible " << (std::is_trivially_default_constructible<decltype(n)>::value == true ? "yes" : "no") << std::endl;
                std::cout << "  is_trivially_copy_constructible    " << (std::is_trivially_copy_constructible<decltype(n)>::value == true ? "yes" : "no") << std::endl;
                std::cout << "  is_trivially_move_constructible    " << (std::is_trivially_move_constructible<decltype(n)>::value == true ? "yes" : "no") << std::endl;
                std::cout << "  is_trivially_copy_assignable       " << (std::is_trivially_copy_assignable<decltype(n)>::value == true ? "yes" : "no") << std::endl;
                std::cout << "  is_trivially_move_assignable       " << (std::is_trivially_move_assignable<decltype(n)>::value == true ? "yes" : "no") << std::endl;
                std::cout << "  is_trivially_destructible          " << (std::is_trivially_destructible<decltype(n)>::value == true ? "yes" : "no") << std::endl;
                std::cout << std::endl;
            }

            if constexpr(showsNodeInfo == true) {
                std::cout << "  [Node properties]" << std::endl;
                std::cout << "  is root     " << n.isRoot();
                std::cout << "  is leaf     " << n.isLeaf();
                std::cout << "  key         " << n.key();
                std::cout << "  parent != 0 " << n.parent() == nullptr;
            }
        }

        template<typename T>
        void analyze(const T& n) const {
            visit<T, true, true, true, false>(n);
        };
    };

    struct PrintVisitor  {
        template<typename T>
        void visit(T& n) const {
            TypeIDPrinterVisitor v;
            v.visit<T, false, false, false, false>(n);
        }
    };

    struct TrieConfigRaw {
        using Key = uint8_t;
        using Value = TrieNode<TrieConfigRaw>*;
        using ContainerType = boost::container::map<Key, Value>;
    };

    struct TrieConfigUnique {
        using Key = uint8_t;
        using Value = std::unique_ptr<TrieNode<TrieConfigUnique>>;
        using ContainerType = std::unordered_map<Key, Value>;
    };

    struct TrieConfigShared {
        using Key = uint8_t;
        using Value = std::shared_ptr<TrieNode<TrieConfigShared>>;
        using ContainerType = std::unordered_map<Key, Value>;
    };

    struct TrieConfigBoostShared {
        using Key = uint8_t;
        using Value = std::shared_ptr<TrieNode<TrieConfigBoostShared>>;
        using ContainerType = boost::container::map<Key, Value>;
    };

    struct TrieRobinRaw {
        using Key = uint8_t;
        using Value = TrieNode<TrieRobinRaw>*;
        using ContainerType = tsl::robin_map<Key, Value>;
    };

    struct TrieRobinUnique {
        using Key = uint8_t;
        using Value = std::unique_ptr<TrieNode<TrieRobinUnique>>;
        using ContainerType = tsl::robin_map<Key, Value>;
    };

    struct TrieRobinShared {
        using Key = uint8_t;
        using Value = std::shared_ptr<TrieNode<TrieRobinShared>>;
        using ContainerType = tsl::robin_map<Key, Value>;
    };

    auto CustomDeleter = []<dust::trie::inspect::RawPointer P>(P p) {
        //std::cout << "[CustomDeleter] Deleting value is : " << typeid(p).name() << " Key: " << p->key() << std::endl;
        if constexpr (dust::trie::inspect::IsRawPointer<P>::value == true) { // MSVC 2019
            delete p;
            return;
        }
        
        assert(false);
    };

    struct TrieRobinUniqueWithDeleter {
        using Key = uint8_t;
        using Value = std::unique_ptr<TrieNode<TrieRobinUniqueWithDeleter>, decltype(CustomDeleter)>;
        using ContainerType = tsl::robin_map<Key, Value>;
    };

    struct TrieRobinUniqueVirtual {
        using Key = uint8_t;
        using Value = std::unique_ptr<dust::trie::rct::IVisitableNode<TrieRobinUniqueVirtual>>;
        using ContainerType = tsl::robin_map<Key, Value>;
        using Root = dust::trie::rct::NodeRoot<TrieRobinUniqueVirtual>;
        using Container = dust::trie::rct::NodeContainer<TrieRobinUniqueVirtual>;
        using Terminator = dust::trie::rct::NodeTerminator<TrieRobinUniqueVirtual>;
    };

    struct TrieRobinUniqueVirtualWithDeleter {
        using NodeInterface = dust::trie::rct::IVisitableNode<TrieRobinUniqueVirtualWithDeleter>;
        using Key = uint8_t;
        using Value = std::unique_ptr< NodeInterface, std::function<void (NodeInterface*)> >;
        using ContainerType = tsl::robin_map<Key, Value>;
    };

    struct HopscotchMapUnique {
        using Key = uint8_t;
        using Value = std::unique_ptr<TrieNode<HopscotchMapUnique>>;
        using ContainerType = tsl::bhopscotch_map<Key, Value>;
    };

    struct HopscotchMapShared {
        using Key = uint8_t;
        using Value = std::shared_ptr<TrieNode<HopscotchMapUnique>>;
        using ContainerType = tsl::bhopscotch_map<Key, Value>;
    };
}

namespace dust::test::dictionary {
    std::vector<std::string> oneWord = { "fran", };
    std::vector<std::string> twoWords = { "fran", "francesca" };
    std::vector<std::string> bunchOfWords = { "fran", "francesca", "frana" };

    std::vector<std::string> overlappingWords1 = { "fran", "fr4n", "fra1" };
    std::vector<std::string> overlappingWords2 = { "abcd1", "abde1", "adef1" };
    std::vector<std::string> overlappingWords3 = { "a1", "a2", "a11", "b1" };

    std::vector<std::string> smallWordsList = {
        "fran",
        "francare", "francato", "francatura", "francature", "francesca", 
        "francescana", "francescane", "francescanesimi", "francescanesimo",
        "wolframio", "wolframite","woman","women","woofer","woolf","workshop","workstation","wrestling","writer",
        "wuerstel","wundt","wyeth","wyoming","xantato","xantene","xantenico","xanthi","xantica","xantiche","xantici",
        "xantico"
    };

    std::vector<std::string> mediumWordsList = {
        "ability", "able", "about", "above", "accept", "according", "account", "across", "act", "action", "activity", "actually", "add", "address", "administration", "admit", "adult", "affect", "after", "again", "against", "age","agency", "agent", "ago",
        "agree", "agreement", "ahead", "air", "all", "allow", "almost", "alone", "along", "already", "also", "although", "always", "american", "among", "amount", "analysis", "and", "animal", "another", "answer", "any", "anyone","anything", "appear",
        "apply", "approach", "area", "argue", "arm", "around", "arrive", "art", "article", "artist", "as", "ask", "assume", "at", "attack", "attention", "attorney", "audience", "author", "authority", "available", "avoid", "away","baby", "back",
        "bad", "bag", "ball", "bank", "bar", "base", "be", "beat", "beautiful", "because", "become", "bed", "before", "begin", "behavior", "behind", "believe", "benefit", "best", "better", "between", "beyond", "big", "bill", "billion","bit", "black", "blood", "blue", "board",
        "body", "book", "born", "both", "box", "boy", "break", "bring", "brother", "budget", "build", "building", "business", "but", "buy", "by", "call", "camera", "campaign", "can", "cancer", "candidate", "capital", "car", "card","care", "career", "carry", "case", "catch",
        "cause", "cell", "center", "central", "century", "certain", "certainly", "chair", "challenge", "chance", "change", "character", "charge", "check", "child", "choice", "choose", "church", "citizen", "city", "civil", "claim","class", "clear", "clearly",
        "close", "coach", "cold", "collection", "college", "color", "come", "commercial", "common", "community", "company", "compare", "computer", "concern", "condition", "conference", "congress", "consider", "consumer", "contain", "continue", "control", "cost", "could", "country","couple", "course", "court", "cover", "create",
        "crime", "cultural", "culture", "cup", "current", "customer", "cut", "dark", "data", "daughter", "day", "dead", "deal", "death", "debate", "decade", "decide", "decision", "deep", "defense", "degree", "democrat", "democratic","describe", "design",
        "despite", "detail", "determine", "develop", "development", "die", "difference", "different", "difficult", "dinner", "direction", "director", "discover", "discuss", "discussion", "disease", "do", "doctor", "dog", "door", "down","draw", "dream", "drive", "drop",
        "drug", "during", "each", "early", "east", "easy", "eat", "economic", "economy", "edge", "education", "effect", "effort", "eight", "either", "election", "else", "employee", "end", "energy", "enjoy", "enough", "enter", "entire","environment",
        "environmental", "especially", "establish", "even", "evening", "event", "ever", "every", "everybody", "everyone", "everything", "evidence", "exactly", "example", "executive", "exist", "expect", "experience", "expert", "explain", "eye", "face", "fact", "factor", "fail","fall", "family", "far", "fast", "father",
        "fear", "federal", "feel", "feeling", "few", "field", "fight", "figure", "fill", "film", "final", "finally", "financial", "find", "fine", "finger", "finish", "fire", "firm", "first", "fish", "five", "floor", "fly", "focus", "follow", "food", "foot", "for", "force",
        "foreign", "forget", "form", "former", "forward", "four", "free", "friend", "from", "front", "full", "fund", "future", "game", "garden", "gas", "general", "generation", "get", "girl", "give", "glass", "go", "goal", "good", "government", "great", "green", "ground", "group",
        "grow", "growth", "guess", "gun", "guy", "hair", "half", "hand", "hang", "happen", "happy", "hard", "have", "he", "head", "health", "hear", "heart", "heat", "heavy", "help", "her", "here", "herself", "high", "him", "himself", "his", "history", "hit",
        "hold", "home", "hope", "hospital", "hot", "hotel", "hour", "house", "how", "however", "huge", "human", "hundred", "husband", "idea", "identify", "if", "image", "imagine", "impact", "important", "improve", "in", "include", "including", "increase", "indeed", "indicate", "individual",
        "industry", "information", "inside", "instead", "institution", "interest", "interesting", "international", "interview", "into", "investment", "involve", "issue", "it", "item", "its", "itself", "job", "join", "just", "keep", "key", "kid", "kill", "kind",
        "kitchen", "know", "knowledge", "land", "language", "large", "last", "late", "later", "laugh", "law", "lawyer", "lay", "lead", "leader", "learn", "least", "leave", "left", "leg", "legal", "less", "let", "letter", "level", "lie", "life", "light", "like", "likely", "line", "list", "listen", "little", "live", "local", "long", "look", "lose", "loss", "lot", "love", "low", "machine", "magazine", "main", "maintain", "major", "majority", "make", "man", "manage", "management", "manager", "many", "market", "marriage", "material", "matter", "may", "maybe", "me", "mean", "measure", "media", "medical", "meet", "meeting", "member", "memory", "mention", "message", "method", "middle", "might",
        "military", "million", "mind", "minute", "miss", "mission", "model", "modern", "moment", "money", "month", "more", "morning", "most", "mother", "mouth", "move", "movement", "movie", "mr", "mrs", "much", "music", "must", "my", "myself", "name", "nation", "national", "natural",
        "nature", "near", "nearly", "necessary", "need", "network", "never", "new", "news", "newspaper", "next", "nice", "night", "no", "none", "nor", "north", "not", "note", "nothing", "notice", "now", "n't", "number", "occur", "of", "off", "offer", "office", "officer", "official", "often", "oh", "oil", "ok", "old", "on", "once", "one", "only", "onto", "open", "operation", "opportunity", "option", "or", "order", "organization", "other", "others", "our", "out", "outside", "over", "own", "owner", "page", "pain", "painting", "paper", "parent", "part", "participant", "particular", "particularly", "partner", "party", "pass", "past", "patient", "pattern", "pay", "peace", "people", "per",
        "perform", "performance", "perhaps", "period", "person", "personal", "phone", "physical", "pick", "picture", "piece", "place", "plan", "plant", "play", "player", "pm", "point", "police", "policy", "political", "politics", "poor", "popular", "population",
        "position", "positive", "possible", "power", "practice", "prepare", "present", "president", "pressure", "pretty", "prevent", "price", "private", "probably", "problem", "process", "produce", "product", "production", "professional", "professor", "program", "project", "property", "protect", "prove", "provide", "public", "pull", "purpose",
        "push", "put", "quality", "question", "quickly", "quite", "race", "radio", "raise", "range", "rate", "rather", "reach", "read", "ready", "real", "reality", "realize", "really", "reason", "receive", "recent", "recently", "recognize", "record",
        "red", "reduce", "reflect", "region", "relate", "relationship", "religious", "remain", "remember", "remove", "report", "represent", "require", "research", "resource", "respond", "response", "responsibility", "rest", "result", "return", "reveal", "rich", "right", "rise", "risk", "road", "rock", "role",
        "room", "rule", "run", "safe", "same", "save", "say", "scene", "school", "science", "scientist", "score", "sea", "season", "seat", "second", "section", "security", "see", "seek", "seem", "sell", "send", "senior", "sense", "series", "serious", "serve", "service", "set",
        "seven", "several", "sex", "sexual", "shake", "share", "she", "shoot", "short", "shot", "should", "shoulder", "show", "side", "sign", "significant", "similar", "simple", "simply", "since", "sing", "single", "sister", "sit", "site",
        "situation", "six", "size", "skill", "skin", "small", "smile", "so", "social", "society", "soldier", "some", "somebody", "someone", "something", "sometimes", "son", "song", "soon", "sort", "sound", "source", "south", "southern", "space",
        "speak", "special", "specific", "speech", "spend", "sport", "spring", "staff", "stage", "stand", "standard", "star", "start", "state", "statement", "station", "stay", "step", "still", "stock", "stop", "store", "story", "strategy", "street",
        "strong", "structure", "student", "study", "stuff", "style", "subject", "success", "successful", "such", "suddenly", "suffer", "suggest", "summer", "support", "sure", "surface", "system", "table", "take", "talk", "task", "tax", "teach", "teacher",
        "team", "technology", "television", "tell", "ten", "tend", "term", "test", "than", "thank", "that", "the", "their", "them", "themselves", "then", "theory", "there", "these", "they", "thing", "think", "third", "this", "those", "though", "thought", "thousand", "threat", "three",
        "through", "throughout", "throw", "thus", "time", "to", "today", "together", "tonight", "too", "top", "total", "tough", "toward", "town", "trade", "traditional", "training", "travel", "treat", "treatment", "tree", "trial", "trip", "trouble",
        "true", "truth", "try", "turn", "tv", "two", "type", "under", "understand", "unit", "until", "up", "upon", "us", "use", "usually", "value", "various", "very", "victim", "view", "violence", "visit", "voice", "vote", "wait", "walk", "wall", "want", "war", "watch", "water", "way", "we", "weapon", "wear", "week", "weight", "well", "west", "western", "what", "whatever", "when", "where", "whether", "which", "while", "white", "who", "whole", "whom", "whose", "why", "wide", "wife", "will", "win", "wind", "window", "wish", "with", "within", "without", "woman", "wonder", "word", "work", "worker", "world", "worry", "would", "write", "writer", "wrong", "yard", "yeah", "year", "yes", "yet",
        "you", "young", "your", "yourself"
    };
}

namespace dust::test::adapters {

    template<typename T>
    class BoostUnorderedMapAdapter : T {
            using T::cend;
            
        public:
            using typename T::key_type;
            using typename T::mapped_type;
            using typename T::size_type;
            using T::operator[];
            using T::find;
            using T::size;
            using T::insert_or_assign;

            bool contains( const key_type& key ) const {
                return find(key) != cend();
            }
            template< class K >
            bool contains( const K& x ) const {
              return find(x) != cend();
            }
    };
}
