#define CATCH_CONFIG_ENABLE_BENCHMARKING
#include <catch.hpp>

#include <dust/ds/trie.h>
#include <dust/ds/vtrie/vtrie_rct.h>

#include "../test_profiles.h"

TEST_CASE("Benchmark trie", "[benchmark] [rawpointers]") {
    using namespace dust::trie;
    SECTION("With raw pointers and unordered_map") {
        BENCHMARK("unordered 1") {};
        BENCHMARK("unordered 2") {};
    };
    SECTION("With raw pointers and ordered_map") {
        BENCHMARK("ordered 1") {};
        BENCHMARK("ordered 2") {};
    };
};

/*
TEST_CASE("Maps benchmark", "[benchmark]") {
    SECTION("With UniqueNode") {
        using namespace dust::test;
        using namespace dust::trie;
        using NodePtr = TrieRobinUnique::Value;
        using NodeRaw = std::pointer_traits<NodePtr>::element_type;
      
        std::vector<std::string> dictionary(dictionary::mediumWordsList);
        auto operationInsertDictionary = [&dictionary](NodeRaw& n) {
            std::transform(dictionary.begin(), dictionary.end(), dictionary.begin(),
                      [](auto c) -> auto { return c + ' '; });
            OperationInsert<TrieRobinUnique>(dictionary, n);
        };

        NodePtr node = TrieRobinUnique::Factory::createRoot();
        VisitorLambda visitorInsert(operationInsertDictionary);
        node->accept(visitorInsert);

        BENCHMARK("UniqueNode VisitorRetrieveAll<TrieRobinUnique>") {
            VisitorRetrieveAll<TrieRobinUnique> v;
            node->accept(v);
        };
    };

    SECTION("With pointers and unordered_map") {
        using namespace dust::test;
        using namespace dust::trie;
        using NodePtr = TrieConfigRaw::Value;
        using NodeRaw = std::pointer_traits<NodePtr>::element_type;
      
        std::vector<std::string> dictionary(dictionary::mediumWordsList);
        auto operationInsertDictionary = [&dictionary](NodeRaw& n) {
            std::transform(dictionary.begin(), dictionary.end(), dictionary.begin(),
                      [](auto c) -> auto { return c + ' '; });
            OperationInsert<TrieConfigRaw>(dictionary, n);
        };

        NodePtr node = TrieConfigRaw::Factory::createRoot();
        VisitorLambda visitorInsert(operationInsertDictionary);
        node->accept(visitorInsert);

        BENCHMARK("UniqueNode VisitorRetrieveAll<TrieConfigRaw>") {
            VisitorRetrieveAll<TrieConfigRaw> v;
            node->accept(v);
        };
    };
};
*/