#include <catch.hpp>

#include <dust/ds/cx/trie.h>

#include <array>
#include <tuple>
#include <limits>
#include <memory>

#include <unordered_map>
//#include <frozen/map.h>
//#include <frozen/unordered_map.h>
#include <boost/graph/graph_traits.hpp>

namespace dust::ds::cx {

template <typename T>
class TestVisitor {
  public:
    virtual void visit(const T& n) {
      if (n.value() == '.') {
         s += ' ';
         std::cout << s  << std::endl;
         s.clear();
         return;
      }

      using Container = typename T::Container;
      const Container& c = n.container();
      s += std::string(1, n.value());
      std::string copy;

      for (const auto& o : c) {
        copy = s;
        o.second->accept(*this);
        s = copy;
      }
    }

  private:
    std::string s;
};
}


TEST_CASE( "TrieNode creation", "[test01]" ) {
    using namespace dust::ds::cx;

    using Node = TrieNode<char>;
    using Root = TrieNode<char>;
    std::unique_ptr<Node> dict = std::make_unique<Root>();
    std::vector<std::string> words = {
        "fran",
        "francare", "francato", "francatura", "francature", "francesca", "francesca",
        "francescana", "francescane", "francescanesimi", "francescanesimo",
        "wolframio", "wolframite","woman","women","woofer","woolf","workshop","workstation","wrestling","writer",
        "wuerstel","wundt","wyeth","wyoming","xantato","xantene","xantenico","xanthi","xantica","xantiche","xantici",
        "xantico"
    };

    for (const std::string& s : words) {
        std::string s_terminated = s + ".";
        dict->insert(s_terminated);
    }

    TestVisitor<Node> tv;
    dict->accept(tv);

    dict->insert('z');
    auto sub = dict->subtrie("fra");
    
    std::cout << std::endl << "Printing dict: " << std::endl;
    dict->print();

    std::cout << std::endl << "Printing sub: " << std::endl;
    sub->print();
    
    int i = 0;
}
