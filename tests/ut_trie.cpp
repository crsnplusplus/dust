#include <catch.hpp>

#include <dust/ds/trie.h>
#include <dust/ds/vtrie/vtrie_rct.h>

#include "test_profiles.h"


TEST_CASE("inspect::IsMapCompatible") {
    using namespace dust::trie;
    using dust::test::adapters::BoostUnorderedMapAdapter;

    using BoostUnorderedMap = BoostUnorderedMapAdapter<boost::unordered_map<int, int*>>;

    STATIC_REQUIRE( inspect::IsMapCompatible<bool>() == false );
    STATIC_REQUIRE( inspect::IsMapCompatible<std::list<int>>() == false );
    STATIC_REQUIRE( inspect::IsMapCompatible<std::vector<int>>() == false );
    STATIC_REQUIRE( inspect::IsMapCompatible<std::list<int>>() == false );
    STATIC_REQUIRE( inspect::IsMapCompatible<std::vector<int>>() == false );
    STATIC_REQUIRE( inspect::IsMapCompatible<std::map<int, int*>>() );
    STATIC_REQUIRE( inspect::IsMapCompatible<std::unordered_map<int, int*>>() );
    STATIC_REQUIRE( inspect::IsMapCompatible<boost::container::map<int, int*>>() );
    // STATIC_REQUIRE ( skills::HasMapInterface<BoostUnorderedMap>() ); 
    // no contains, insert_or_assign methods. operator[] not working. needs adapter
    // STATIC_REQUIRE ( skills::HasMapInterface<frozen::map<int, int*, 10>>() ); 
    // STATIC_REQUIRE ( skills::HasMapInterface<frozen::unordered_map<int, int*, 10>>() ); 
}

TEST_CASE("inspect::IsUniquePointer") {
    using namespace dust::trie;
    using namespace dust::test;

    STATIC_REQUIRE( inspect::IsUniquePointer<std::list<int>>() == false );
    STATIC_REQUIRE( inspect::IsUniquePointer<std::vector<int>>() == false );
    STATIC_REQUIRE( inspect::IsUniquePointer<int>() == false );
    STATIC_REQUIRE( inspect::IsUniquePointer<TrieRobinUnique::Value>() );
    STATIC_REQUIRE( inspect::IsUniquePointer<TrieRobinUniqueWithDeleter::Value>() );
    STATIC_REQUIRE( inspect::IsUniquePointer<TrieConfigShared::Value>() == false );
    STATIC_REQUIRE( inspect::IsUniquePointer<TrieRobinShared::Value>() == false );
    STATIC_REQUIRE( inspect::IsUniquePointer<HopscotchMapUnique::Value>() == true);
    STATIC_REQUIRE( inspect::IsUniquePointer<std::weak_ptr<int>>() == false );
}

TEST_CASE("inspect::IsSharedPointer") {
    using namespace dust::trie;
    using namespace dust::test;
    
    STATIC_REQUIRE( inspect::IsSharedPointer<std::list<int>>() == false );
    STATIC_REQUIRE( inspect::IsSharedPointer<std::vector<int>>() == false );
    STATIC_REQUIRE( inspect::IsSharedPointer<TrieConfigShared::Value>() == true );
    STATIC_REQUIRE( inspect::IsSharedPointer<TrieConfigBoostShared::Value>() );
    STATIC_REQUIRE( inspect::IsSharedPointer<TrieRobinRaw::Value>() == false );
    STATIC_REQUIRE( inspect::IsSharedPointer<TrieRobinUnique::Value>() == false );
    STATIC_REQUIRE( inspect::IsSharedPointer<TrieRobinShared::Value>() == true);
    STATIC_REQUIRE( inspect::IsSharedPointer<TrieRobinUniqueWithDeleter::Value>() == false );
    STATIC_REQUIRE (inspect::IsSharedPointer<HopscotchMapShared::Value>() == true);
    STATIC_REQUIRE( inspect::IsSharedPointer<std::weak_ptr<int>>() == false );
}

TEST_CASE("Raw pointers creation and destruction") {
    using namespace dust::trie;
    using RawNode = TrieNode<dust::test::TrieRobinRaw>;
    RawNode* raw_root_with_parent = new RawNode;
    auto raw_node_with_parent = new TrieNode<dust::test::TrieRobinRaw>('F', raw_root_with_parent);
    auto rnp = raw_node_with_parent->parent();
    REQUIRE(raw_root_with_parent == rnp);
    delete raw_node_with_parent;
    delete raw_root_with_parent;
}


TEST_CASE( "Trie of unique pointers detach" ) {
    using namespace dust::trie;
    using UniqueNode = dust::test::TrieRobinUnique::Value;
    using UniqueNodePointer = UniqueNode::pointer;

    UniqueNode unique_root_with_parent { new typename UniqueNode::element_type(' ', nullptr) };
    UniqueNode newRoot { new typename UniqueNode::element_type(' ', nullptr) };
}


TEST_CASE("Insertion test") {

}

TEST_CASE("Visitor lambda") {
    using namespace dust::test;
    using namespace dust::trie;
    using namespace dust::trie::visitors;

    using NodePtr = TrieRobinUnique::Value;
    using NodeRaw = std::pointer_traits<NodePtr>::element_type;
    using Factory = factory::Factory<NodePtr, NodeRaw>;
    NodePtr root = Factory::make(0, nullptr);
    root->add(Factory::make('F', root.get()));

    auto capturedKey = 0;
    auto LambdaPredicateExtractKey = [&capturedKey](const auto& n) { capturedKey=n.key(); };
    VisitorLambda visitor(LambdaPredicateExtractKey);
    root->get('F')->accept(visitor);
    
    REQUIRE(capturedKey == root->get('F')->key());
}
/*
TEST_CASE("Visitor lambda insert") {
    using namespace dust::test;
    using namespace dust::trie;
    using NodePtr = TrieRobinUnique::Value;
    using NodeRaw = std::pointer_traits<NodePtr>::element_type;
  
    std::vector<std::string> dictionary(dictionary::smallWordsList);

    auto insertDictionary = [&dictionary](NodeRaw& n) {
        OperationInsert<TrieRobinUnique>(dictionary, n);
    };

    NodePtr node = TrieRobinUnique::Factory::createRoot();
    VisitorLambda visitor(insertDictionary);
    node->accept(visitor);
}

TEST_CASE("Visitor lambda retrieve") {
    using namespace dust::test;
    using namespace dust::trie;
    using NodePtr = TrieRobinUnique::Value;
    using NodeRaw = std::pointer_traits<NodePtr>::element_type;
  
    std::vector<std::string> dictionary(dictionary::mediumWordsList);

    auto operationInsertDictionary = [&dictionary](NodeRaw& n) {
        std::transform(dictionary.begin(), dictionary.end(), dictionary.begin(),
            [](auto c) -> auto { return c + ' '; });
        OperationInsert<TrieRobinUnique>(dictionary, n);
    };

    NodePtr node = TrieRobinUnique::Factory::createRoot();
    VisitorLambda visitorInsert(operationInsertDictionary);
    node->accept(visitorInsert);

    VisitorRetrieveAll<TrieRobinUnique> v;
    node->accept(v);

    for (const auto& word : v.collected()) {
        for (const auto& c : word) {
            std::cout << c;
        }
        std::cout << std::endl;
    }
}
*/


TEST_CASE("Trie V test") {
    using namespace dust;
    using namespace dust::trie::rct;
    using TC = test::TrieRobinUniqueVirtual;
    using NodeInterface = IVisitableNode<TC>;
    using NodePtr = std::unique_ptr<NodeInterface>;
    using Root = NodeRoot<TC>;
    using Container = NodeContainer<TC>;
    using Terminator = NodeTerminator<TC>;

    struct Visitor : public IVisitor<TC> {
        virtual void visitRoot(Root& r) {
            std::cout << "Root" << std::endl;
        }
        virtual void visitContainer(Container& c) {
            std::cout << "Container" << c.key() << std::endl;
        }
        virtual void visitTerminator(Terminator& t) {
            std::cout << "Terminator" << std::endl;
        }
        virtual ~Visitor() {}
    };
    Visitor v;

    NodePtr root = std::make_unique<Root>();
    NodePtr node1 = std::make_unique<Container>('1');
    NodePtr node2 = std::make_unique<Container>('2');
    NodePtr node3 = std::make_unique<Container>('3');
    NodePtr terminator = std::make_unique<Terminator>();
    std::vector<NodeInterface*> nodes { root.get(), node1.get(), node2.get(), node3.get(), terminator.get() };

    root->add(std::move(node1))
        ->add(std::move(node2))
        ->add(std::move(node3))
        ->add(std::move(terminator));

    root->add(std::move(std::make_unique<Container>('A')));
    root->add(std::make_unique<Container>('B'));
   
    for (auto& n : nodes) {
        n->accept(v);
    }

    std::vector<NodePtr*> nodes2{&root};
    (**nodes2.front()).accept(v);
}

TEST_CASE("Trie Factory test") {
    using namespace dust::trie::rct;
    struct TrieRobinUniqueVirtual {
        using Key = uint8_t;
        using Value = std::unique_ptr<IVisitableNode<TrieRobinUniqueVirtual>>;
        using ContainerType = tsl::robin_map<Key, Value>;
        using Userdata = void;
    };

    using TC = TrieRobinUniqueVirtual;
    using NodeInterface = IVisitableNode<TC>;

    auto CustomDeleter = [](auto p) {
        //std::cout << "[CustomDeleter] Deleting value is : " << typeid(p).name() << " Key: " << p->key() << std::endl;
        delete p;    
    };

    using NodePtr = std::unique_ptr<NodeInterface>;
    using Root = NodeRoot<TC>;
    using Container = NodeContainer<TC>;
    using Terminator = NodeTerminator<TC>;

    {
        using RootAllocator = std::allocator<Root>;
        RootAllocator rootAllocator;
        Root* xRoot = rootAllocator.allocate(1);
        std::allocator_traits<RootAllocator>::construct(rootAllocator, xRoot);
        std::unique_ptr<NodeInterface> a(xRoot);
    }

    {
        using ContainerAllocator = std::allocator<Container>;
        ContainerAllocator containerAllocator;
        Container* xContainer = containerAllocator.allocate(1);
        std::allocator_traits<ContainerAllocator>::construct(containerAllocator, xContainer, '0');
        
        auto CustomDeleterContainer = [&containerAllocator](NodeInterface* p) {
            //std::cout << "[CustomDeleter] Deleting value is : " << typeid(p).name() << " Key: " << p->key() << std::endl;
            containerAllocator.deallocate(static_cast<Container*>(p), 1);
        };
        auto cd = CustomDeleter;
        std::unique_ptr<NodeInterface, decltype(CustomDeleter)> a(xContainer, cd);
    }
}

TEST_CASE("Trie virtual UniquePtr with allocator factory test") {
    using namespace dust::trie::rct;

    using TC = dust::test::TrieRobinUniqueVirtualWithDeleter;
    using NodeInterface = IVisitableNode<TC>;
    using NodePtr = std::unique_ptr<NodeInterface>;
    using Root = NodeRoot<TC>;
    using Container = NodeContainer<TC>;
    using Terminator = NodeTerminator<TC>;

    using RootAllocator = std::allocator<Root>;
    RootAllocator rootAllocator;
    Root* xRoot = rootAllocator.allocate(1);
    std::allocator_traits<RootAllocator>::construct(rootAllocator, xRoot);

    using ContainerAllocator = std::allocator<Container>;
    ContainerAllocator containerAllocator;
    Container* xContainer = containerAllocator.allocate(1);
    std::allocator_traits<ContainerAllocator>::construct(containerAllocator, xContainer, '7');

    using TerminatorAllocator = std::allocator<Terminator>;
    TerminatorAllocator terminatorAllocator;
    Terminator* xTerminator = terminatorAllocator.allocate(1);
    std::allocator_traits<TerminatorAllocator>::construct(terminatorAllocator, xTerminator);

    std::function<void(NodeInterface*)> fDeleterGeneric = [](NodeInterface* p) { delete p; };
    std::function<void(NodeInterface*)> fDeleterRoot = [&rootAllocator](NodeInterface* p) {
        std::cout << "[CustomDeleter] Deleting Root: " << sizeof( static_cast<Root&>(*p) )  << std::endl;
        rootAllocator.deallocate(static_cast<Root*>(p), 1);
    };
    std::function<void(NodeInterface*)> fDeleterContainer = [&containerAllocator](NodeInterface* p) {
        std::cout << "[CustomDeleter] Deleting Container : " << sizeof( static_cast<Container&>(*p) ) << " - " << typeid(p).name() << " Key: " << p->key() << std::endl;
        containerAllocator.deallocate(static_cast<Container*>(p), 1);
    };

    std::function<void(NodeInterface*)> fDeleterTerminator = [&terminatorAllocator](NodeInterface* p) {
        std::cout << "[CustomDeleter] Deleting Terminator: " << sizeof( static_cast<Terminator&>(*p) ) << std::endl;
        terminatorAllocator.deallocate(static_cast<Terminator*>(p), 1);
    };

    using MyUniquePtr = std::unique_ptr<NodeInterface, std::function<void (NodeInterface*)> >;
    std::vector<MyUniquePtr> nodes;
    nodes.push_back( std::move(MyUniquePtr(xRoot, fDeleterRoot)) );
    nodes.push_back( std::move(MyUniquePtr(xContainer, fDeleterContainer)) );
    nodes.push_back( std::move(MyUniquePtr(new Container('5'), fDeleterGeneric)) );
    nodes.push_back( std::move(MyUniquePtr(xTerminator, fDeleterTerminator)) );
    
    dust::test::TypeIDPrinterVisitor tipv;
    tipv.visit(fDeleterGeneric);

    struct Visitor : public IVisitor<TC> {
        virtual void visitRoot(Root& r) {
            std::cout << "Visit root" << std::endl;
        }
        virtual void visitContainer(Container& c) {
            std::cout << "Visit container with key: " << c.key() << std::endl;
        }
        virtual void visitTerminator(Terminator& t) {
            std::cout << "Visit terminator" << std::endl;
        }
        virtual ~Visitor() {}
    };
    Visitor v;

    for (auto& n : nodes) {
        n->accept(v);
    }

    union Fail {
        using Key = long long int;
        using Value = std::unique_ptr<IVisitableNode<dust::test::TrieRobinUniqueVirtualWithDeleter>>;
        using ContainerType = tsl::robin_map<Key, Value>;
        //using ContainerType = boost::container::map<Key, Value>;
        using Userdata = void;
    };
    //static_assert(dust::trie::inspect::IsConfigValid<Fail>());

    std::allocator<Root> allocR;
    std::allocator<Container> allocC;
    std::allocator<Terminator> allocT;
    using Node = std::unique_ptr<NodeInterface, std::function<void (NodeInterface*)>>;
    Node x = dust::trie::factory::makeUniqueWithAllocator<Root, NodeInterface>(allocR);
    Node y = dust::trie::factory::makeUniqueWithAllocator<Container, NodeInterface>(allocC, '1');
    Node z = dust::trie::factory::makeUniqueWithAllocator<Terminator, NodeInterface>(allocT);

    std::vector<Node> someNodes;
    someNodes.push_back( std::move(x) );
    someNodes.push_back( std::move(y) );
    //someNodes.push_back( std::move(z) );
    for (auto& n : someNodes) {
        n->accept(v);
    }

    using namespace dust::trie::factory;
    std::function<void(NodeInterface*)> fDeleter = [](NodeInterface* p) {
        delete p;
    };

    auto ff = makeSharedWithDeleter<NodeInterface, Root, decltype(fDeleter)>(fDeleter);
}

TEST_CASE("Trie factory::Factory Raw") {
    using TC = dust::test::TrieConfigRaw;
    using NodePtr = typename TC::Value;
    using namespace dust::trie;

    auto x = factory::Factory<NodePtr>::make(0, nullptr);
    delete x;
}

TEST_CASE("Trie factory::Factory Unique") {
    using TC = dust::test::TrieConfigUnique;
    using NodePtr = typename TC::Value;
    using namespace dust::trie;

    auto x = factory::Factory<NodePtr>::make(0, nullptr);
    auto y = factory::Factory<NodePtr>::make();
}

TEST_CASE("Trie factory::Factory Shared") {
    using TC = dust::test::TrieConfigShared;
    using NodePtr = typename TC::Value;
    using namespace dust::trie;

    auto x = factory::Factory<NodePtr>::make(0, nullptr);
    auto y = factory::Factory<NodePtr>::make();
}

TEST_CASE("Trie factory::Factory with deleter Unique") {
    using namespace dust;
    using namespace dust::trie;
    using namespace dust::trie::factory;

    using TC = test::TrieRobinUniqueWithDeleter;
    using NodePtr = typename TC::Value;
    using NodeRaw = inspect::PointerExtractor<NodePtr>::ElementType;
    std::allocator<NodeRaw> allocator;

    auto deleter = test::CustomDeleter;
    using DeleterType = decltype(deleter);
    auto x = factory::FactoryWithDeleter<NodePtr, NodeRaw, DeleterType>::make(deleter, 0, nullptr);
    auto y = factory::FactoryWithDeleter<NodePtr, NodeRaw, DeleterType>::make(deleter);
}

TEST_CASE("Trie factory::Factory with deleter Shared") {
    using namespace dust;
    using namespace dust::trie;
    using namespace dust::trie::factory;

    using TC = test::TrieConfigShared;
    using NodePtr = typename TC::Value;
    using NodeRaw = inspect::PointerExtractor<NodePtr>::ElementType;
    std::allocator<NodeRaw> allocator;

    auto deleter = test::CustomDeleter;
    using DeleterType = decltype(deleter);
    auto x = factory::FactoryWithDeleter<NodePtr, NodeRaw, DeleterType>::make(deleter, 0, nullptr);
    auto y = factory::FactoryWithDeleter<NodePtr, NodeRaw, DeleterType>::make(deleter);
}

TEST_CASE("Trie factory::Factory with allocator Raw") {
    using namespace dust;
    using namespace dust::trie;
    using TC = test::TrieConfigRaw;
    using NodePtr = typename TC::Value;
    using NodeElement = inspect::PointerExtractor<NodePtr>::ElementType;
    std::allocator<NodeElement> allocator;
    auto x = factory::FactoryWithAllocator<NodePtr>::make(allocator, 0, nullptr);
    auto y = factory::FactoryWithAllocator<NodePtr>::make(allocator);
    delete x;
    delete y;
}

TEST_CASE("Trie factory::Factory with allocator Unique") {
    using namespace dust;
    using namespace dust::trie;
    using TC = test::TrieConfigUnique;
    using NodePtr = typename TC::Value;
    using NodeElement = inspect::PointerExtractor<NodePtr>::ElementType;
    std::allocator<NodeElement> allocator;
    auto x = factory::FactoryWithAllocator<NodePtr>::make(allocator, 0, nullptr);
    auto y = factory::FactoryWithAllocator<NodePtr>::make(allocator);
}

TEST_CASE("Trie factory::Factory with allocator Shared") {
    using namespace dust;
    using namespace dust::trie;
    using TC = test::TrieConfigShared;
    using NodePtr = typename TC::Value;
    using NodeElement = inspect::PointerExtractor<NodePtr>::ElementType;
    std::allocator<NodeElement> allocator;
    auto x = factory::FactoryWithAllocator<NodePtr>::make(allocator, 0, nullptr);
    auto y = factory::FactoryWithAllocator<NodePtr>::make(allocator);
}

TEST_CASE("Trie factory::Factory with allocator Boost shared") {
    using namespace dust;
    using namespace dust::trie;
    using TC = test::TrieConfigBoostShared;
    using NodePtr = typename TC::Value;
    using NodeRaw = inspect::PointerExtractor<NodePtr>::ElementType;
    std::allocator<NodeRaw> allocator;
    auto x = factory::FactoryWithAllocator<NodePtr, NodeRaw>::make(allocator, 0, nullptr);
    auto y = factory::FactoryWithAllocator<NodePtr, NodeRaw>::make(allocator);
}

TEST_CASE("Visitor insert sequence") {
    using namespace dust::test;
    using namespace dust::trie;
    using namespace dust::trie;
    using NodePtr = TrieRobinUnique::Value;
    using NodeRaw = std::pointer_traits<NodePtr>::element_type;
    using Factory = factory::Factory<NodePtr, NodeRaw>;

    std::vector<std::string> dictionary(dictionary::smallWordsList);
    NodePtr root = Factory::make(0, nullptr);
    visitors::VisitorInsertSequence<NodePtr, Factory, std::string> v;

    for (const auto& word : dictionary) {
        v.setSequence(word);
        root->accept(v);
    }
}


TEST_CASE("Visitor remove sequence") {
    using namespace dust::test;
    using namespace dust::trie;
    using namespace dust::trie;
    using NodePtr = TrieRobinUnique::Value;
    using NodeRaw = std::pointer_traits<NodePtr>::element_type;
    using Factory = factory::Factory<NodePtr, NodeRaw>;

    std::vector<std::string> dictionary(dictionary::smallWordsList);
    NodePtr root = Factory::make(0, nullptr);
    visitors::VisitorInsertSequence<NodePtr, Factory, std::string> vInsert('#');
    visitors::VisitorFindSequence<NodePtr, Factory, std::string> vFind('#');
    visitors::VisitorRemoveSequence<NodePtr, std::string> vRemove('#');
    visitors::VisitorRetrievePattern<NodePtr, std::string> vRetrieve('#');

    for (const auto& word : dictionary) {
        vInsert.setSequence(word);
        vFind.setSequence(word);
        vRemove.setSequence(word);
        vRetrieve.setSequence(word);

        root->accept(vInsert);
        REQUIRE(root->count() == 1);
        root->accept(vRetrieve);

        root->accept(vFind);
        REQUIRE(vFind.found() == true);

        root->accept(vRemove);
        REQUIRE(root->count() == 0);

        root->accept(vFind);
        REQUIRE(vFind.found() == false);
    }
}


TEST_CASE("Visitor retrieve sequence") {
    using namespace dust::test;
    using namespace dust::trie;
    using namespace dust::trie;
    using NodePtr = TrieRobinUnique::Value;
    using NodeRaw = std::pointer_traits<NodePtr>::element_type;
    using Factory = factory::Factory<NodePtr, NodeRaw>;

    std::vector<std::string> dictionary(dictionary::bunchOfWords);
    NodePtr root = Factory::make(0, nullptr);
    visitors::VisitorInsertSequence<NodePtr, Factory, std::string> vInsert('#');
    visitors::VisitorRetrievePattern<NodePtr, std::string> vRetrieve;

    for (const auto& word : dictionary) {
        vInsert.setSequence(word);
        root->accept(vInsert);
    }

    vInsert.setSequence("fran");
    root->accept(vRetrieve);
}


TEST_CASE("Visitor traversal DFS/BFS") {
    using namespace dust::test;
    using namespace dust::trie;

    using NodePtr = TrieRobinUnique::Value;
    using NodeRaw = std::pointer_traits<NodePtr>::element_type;
    using Key = typename TrieRobinUnique::Key;
    using Sequence = std::vector<Key>;
    using SequenceList = std::vector<Sequence>;
    using Factory = factory::Factory<NodePtr, NodeRaw>;

    std::vector<std::string> dictionary(dictionary::bunchOfWords);
    NodePtr root = Factory::make(0, nullptr);
    visitors::VisitorInsertSequence<NodePtr, Factory, std::string> vInsert('#');
    visitors::VisitorCollectAllSequences<TrieRobinUnique, visitors::StrategyDFS> vCollectDFS;
    visitors::VisitorCollectAllSequences<TrieRobinUnique, visitors::StrategyBFS> vCollectBFS;

    for (const auto& word : dictionary) {
        vInsert.setSequence(word);
        root->accept(vInsert);
    }
    root->accept(vCollectDFS);
    root->accept(vCollectBFS);

    auto terminatorsDFS = vCollectDFS.collectedTerminators();
    auto terminatorsBFS = vCollectBFS.collectedTerminators();

    auto getSequences = [](auto& terminators) {
        SequenceList results;

        for (const auto& t : terminators) {
            NodeRaw* current = t->parent();
            assert(current != nullptr);
            
            Sequence sequence;
            while (current->parent() != nullptr) {
                sequence.insert(sequence.begin(), current->key());
                current = current->parent();
            }
            results.push_back(sequence);
        }

        return std::move(results);
    };

    auto sequencesDFS = getSequences(terminatorsDFS);
    auto sequencesBFS = getSequences(terminatorsBFS);
    REQUIRE( terminatorsDFS.size() == dictionary.size() );
    REQUIRE( terminatorsBFS.size() == dictionary.size() );
}


TEST_CASE("VisitorFindPattern") {
    using namespace dust::test;
    using namespace dust::trie;

    using NodePtr = TrieRobinUnique::Value;
    using NodeRaw = std::pointer_traits<NodePtr>::element_type;
    using Key = typename TrieRobinUnique::Key;
    using Sequence = std::string;//std::vector<Key>;
    using SequenceList = std::vector<Sequence>;
    using Factory = factory::Factory<NodePtr, NodeRaw>;

    std::vector<std::string> dictionary(dictionary::mediumWordsList);
    NodePtr root = factory::Factory<NodePtr, NodeRaw>::make(0, nullptr);
    visitors::VisitorInsertSequence<NodePtr, Factory, std::string> vInsert;

    for (const auto& word : dictionary) {
        vInsert.setSequence(word);
        root->accept(vInsert);
    }

    auto getSequences = [](auto& terminators) {
        SequenceList results;

        for (const auto& t : terminators) {
            NodeRaw* current = t->parent();
            assert(current != nullptr);
            
            Sequence sequence;
            while (current->parent() != nullptr) {
                sequence.insert(sequence.begin(), current->key());
                current = current->parent();
            }
            results.push_back(sequence);
        }

        return std::move(results);
    };

    visitors::VisitorFindPattern<TrieRobinUnique> vFindPattern;
    //std::string pattern = "..p..";
    std::string pattern = "a...";
    vFindPattern.setPattern(pattern);
    root->accept(vFindPattern);

    visitors::VisitorFindSequence<NodePtr, Sequence> vFindSequence;
    vFindSequence.setSequence(pattern);
    root->accept(vFindSequence);

    auto sequencesTerminators = vFindPattern.collected();
    auto foundSequences = getSequences(sequencesTerminators);

    for (auto& sequence : foundSequences) {
        for (auto& e : sequence) {
            //std::cout << e;
        }
        std::cout << sequence << std::endl;
    }
}

