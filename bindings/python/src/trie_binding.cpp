#include <iostream>
#include <string>
#include <vector>
#include <iomanip>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/functional.h>

#include <dust/ds/trie.h>
#include <dust/ds/trie_configs.h>
#include <dust/ds/trie_profiles.h>


namespace py = pybind11;
using Trie = dust::trie::profiles::Trie<dust::trie::config::UniqueStdMap>;
using TrieWithPagesByLengthStd = dust::trie::profiles::TrieWithPagesByLength<dust::trie::config::UniqueStdMap>;
using TrieWithPagesByLengthRobinMap = dust::trie::profiles::TrieWithPagesByLength<dust::trie::config::UniqueRobinMap>;
using TrieWithPagesByLengthHopscotchMap = dust::trie::profiles::TrieWithPagesByLength<dust::trie::config::UniqueHopscotchMap>;


PYBIND11_MODULE(dustpy, m) {
    m.doc() = "Trie in the dust";


    // Trie
    auto pyDustTrie = py::class_<Trie>(
        m,
        "Trie",
        R"pbdoc(Class representing a trie.)pbdoc"
        );
    pyDustTrie
        .def(py::init<>()) // Bind class constructor
        .def(py::init<Trie::Config::Key,
                      Trie::Config::Key>())
        .def("add_word", &Trie::add_word)
        .def("remove_word", &Trie::remove_word)
        .def("find", &Trie::find);


    // TrieDictionary
    auto pyDustTrieWithPagesByLengthStd = py::class_<TrieWithPagesByLengthStd>(
        m,
        "TrieWithPagesByLength",
        R"pbdoc(Class representing a trie.)pbdoc"
        );
    pyDustTrieWithPagesByLengthStd
        .def(py::init<>()) // Bind class constructor
        .def(py::init<TrieWithPagesByLengthStd::Config::Key,
                      TrieWithPagesByLengthStd::Config::Key>())
        .def("add_word", &TrieWithPagesByLengthStd::add_word)
        .def("remove_word", &TrieWithPagesByLengthStd::remove_word)
        .def("find", &TrieWithPagesByLengthStd::find);
    

    // TrieWithPagesByLengthRobinMap
    auto pyDustTrieWithPagesByLengthRobinMap = py::class_<TrieWithPagesByLengthRobinMap>(
        m,
        "TrieWithPagesByLengthRobinMap",
        R"pbdoc(Class representing a trie using a tessil robin map.)pbdoc"
        );
    pyDustTrieWithPagesByLengthRobinMap
        .def(py::init<>()) // Bind class constructor
        .def(py::init<TrieWithPagesByLengthRobinMap::Config::Key,
                      TrieWithPagesByLengthRobinMap::Config::Key>())
        .def("add_word", &TrieWithPagesByLengthRobinMap::add_word)
        .def("remove_word", &TrieWithPagesByLengthRobinMap::remove_word)
        .def("find", &TrieWithPagesByLengthRobinMap::find);


    // TrieWithPagesByLengthHopscotchMap
    auto pyDustTrieWithPagesByLengthHopscotchMap = py::class_<TrieWithPagesByLengthHopscotchMap>(
        m,
        "TrieWithPagesByLengthHopscotchMap",
        R"pbdoc(Class representing a trie using a tessil hopscotch map.)pbdoc"
        );
    pyDustTrieWithPagesByLengthHopscotchMap
        .def(py::init<>()) // Bind class constructor
        .def(py::init<TrieWithPagesByLengthHopscotchMap::Config::Key,
                      TrieWithPagesByLengthHopscotchMap::Config::Key>())
        .def("add_word", &TrieWithPagesByLengthHopscotchMap::add_word)
        .def("remove_word", &TrieWithPagesByLengthHopscotchMap::remove_word)
        .def("find", &TrieWithPagesByLengthHopscotchMap::find);
}
