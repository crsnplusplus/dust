#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <iomanip>

#include <dust/ds/trie.h>
#include <dust/ds/trie_configs.h>
#include <dust/ds/trie_profiles.h>
#include <unordered_map>


using dust::trie::config::RawBoostMap;
using dust::trie::config::RawHopscotchMap;
using dust::trie::config::RawStdMap;
using dust::trie::config::RawRobinMap;

using dust::trie::config::UniqueBoostMap;
using dust::trie::config::UniqueHopscotchMap;
using dust::trie::config::UniqueStdMap;
using dust::trie::config::UniqueRobinMap;

using dust::trie::config::SharedBoostMap;
using dust::trie::config::SharedHopscotchMap;
using dust::trie::config::SharedStdMap;
using dust::trie::config::SharedRobinMap;

using TestConfigs = std::tuple<
    RawBoostMap,
    RawHopscotchMap,
    RawStdMap,
    RawRobinMap,

    UniqueBoostMap,
    UniqueHopscotchMap,
    UniqueStdMap,
    UniqueRobinMap,

    SharedBoostMap,
    SharedHopscotchMap,
    SharedStdMap,
    SharedRobinMap
>;


TEMPLATE_LIST_TEST_CASE("Trie python wrapper - insert", "", TestConfigs) {
    REQUIRE(sizeof(TestType) > 0);
    using Trie = dust::trie::profiles::Trie<TestType>;

    Trie t;
    REQUIRE(t.find("p...").size() == 0);
    t.add_word("pair");
    REQUIRE(t.find("p...").size() == 1);
    t.add_word("pole");
    REQUIRE(t.find("p...").size() == 2);
    t.add_word("pony");
    REQUIRE(t.find("p...").size() == 3);
    REQUIRE(t.find(".o..").size() == 2);
    t.add_word("horse");
    REQUIRE(t.find("....").size() == 3);
    REQUIRE(t.find(".....").size() == 1);
}


TEMPLATE_LIST_TEST_CASE("Trie python wrapper - remove", "", TestConfigs)
{
    REQUIRE(sizeof(TestType) > 0);
    using Trie = dust::trie::profiles::Trie<TestType>;

    Trie t;
    t.add_word("word1");
    t.add_word("word2");
    t.add_word("word3");
    t.add_word("word31");

    t.find(".....");
    REQUIRE(t.find(".....").size() == 3);
    REQUIRE(t.find("......").size() == 1);
    t.remove_word("word3");
    REQUIRE(t.find(".....").size() == 2);
    REQUIRE(t.find("......").size() == 1);
}


TEMPLATE_LIST_TEST_CASE("TrieWithPagesByLength python wrapper - add", "", TestConfigs)
{
    REQUIRE(sizeof(TestType) > 0);
    using dust::trie::profiles::TrieWithPagesByLength;
    using Trie = TrieWithPagesByLength<TestType>;

    Trie t;
    t.add_word("a");
    t.add_word("ab");
    t.add_word("abc");
    t.add_word("abcd");
    t.add_word("abcde");

    REQUIRE(t.find(".").size() == 1);
    REQUIRE(t.find("..").size() == 1);
    REQUIRE(t.find("...").size() == 1);
    REQUIRE(t.find("....").size() == 1);
    REQUIRE(t.find(".....").size() == 1);
}


TEMPLATE_LIST_TEST_CASE("TrieWithPagesByLength python wrapper - add/remove/find", "", TestConfigs)
{
    REQUIRE(sizeof(TestType) > 0);
    using dust::trie::profiles::TrieWithPagesByLength;
    using Trie = TrieWithPagesByLength<TestType>;

    Trie t;
    t.add_word("a");
    t.add_word("ab");
    t.add_word("abc");
    t.add_word("abcd");
    t.add_word("abcde");
    t.add_word("abcdd");
    REQUIRE(t.find(".").size() == 1);
    REQUIRE(t.find("..").size() == 1);
    REQUIRE(t.find("...").size() == 1);
    REQUIRE(t.find("....").size() == 1);
    REQUIRE(t.find(".....").size() == 2);

    t.add_word("a");
    t.add_word("ab");
    t.add_word("abc");
    t.add_word("abcd");
    t.add_word("abcde");
    t.add_word("abcdd");
    REQUIRE(t.find(".").size() == 1);
    REQUIRE(t.find("..").size() == 1);
    REQUIRE(t.find("...").size() == 1);
    REQUIRE(t.find("....").size() == 1);
    REQUIRE(t.find(".....").size() == 2);


    t.remove_word("a");
    REQUIRE(t.find(".").size() == 0);

    t.remove_word("ab");
    REQUIRE(t.find("..").size() == 0);

    t.remove_word("abc");
    REQUIRE(t.find("...").size() == 0);

    t.remove_word("abcd");
    REQUIRE(t.find("....").size() == 0);

    t.remove_word("abcd.");
    // no wildcard support
    REQUIRE(t.find(".....").size() == 2);
}
