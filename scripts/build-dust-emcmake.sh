#!/usr/bin/env bash
SCRIPT_DIR=$(dirname $(realpath "$0"))

source $SCRIPT_DIR/../3rdparty/emsdk/emsdk_env.sh
rm -rf ../build-wasm/*
emcmake cmake -DCMAKE_TOOLCHAIN_FILE=$SCRIPT_DIR/../3rdparty/vcpkg/scripts/buildsystems/vcpkg.cmake \
              -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
              -B../build-wasm/ \
              ..
cd $SCRIPT_DIR/../build-wasm/ && make VERBOSE=1 && cd ../scripts
serve -l 8080 $SCRIPT_DIR/../apps/web-demo/
