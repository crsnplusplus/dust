#!/usr/bin/env bash
SCRIPT_DIR=$(dirname $(realpath "$0"))
cd $SCRIPT_DIR/../build/
cmake .. && make
cd $SCRIPT_DIR/..
