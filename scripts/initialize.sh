#!/usr/bin/env bash
git submodule update --init --recursive
$(dirname "$0")/../3rdparty/vcpkg/bootstrap-vcpkg.sh
$(dirname "$0")/../3rdparty/vcpkg/vcpkg install catch2 robin-map tsl-hopscotch-map frozen boost pybind11